var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var jade = require('gulp-jade');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var connect = require('gulp-connect');
var open = require('gulp-open');
var sourcemaps = require('gulp-sourcemaps');
var ngmin = require('gulp-ngmin');
var minify = require('gulp-minify');
var argv = require('yargs').argv;

var paths = {
  sass: ['./src/scss/**/*.scss'],
  jade: ['./src/jade/**/*.jade'],
  js_other: ['./src/js/*.js'],
  js_controllers: ['./src/js/controllers/**/*.js'],
  root: './www'
};

gulp.task('default', ['sass', 'jade', 'js']);
gulp.task('js', ['js_other', 'js_controllers']);

gulp.task('js_controllers', function() {
  var t = gulp.src(paths.js_controllers)
    .pipe(sourcemaps.init())
    .pipe(concat('controllers.js'))
    .pipe(sourcemaps.write());

  if (!argv.nm) {
    t = t
      .pipe(ngmin())
      .pipe(minify({
        ext:{
          src:'.js',
          min:'.min.js'
        }
      }))
  } else {
    t = t.pipe(rename({suffix: '.min'}))
  }
  return t.pipe(gulp.dest(paths.root + '/js'));
});

gulp.task('js_other', function() {
  var t = gulp.src(paths.js_other);

  if (!argv.nm) {
    t = t
      .pipe(ngmin())
      .pipe(minify({
        ext:{
          src:'.js',
          min:'.min.js'
        }
      }))
  } else {
    t = t.pipe(rename({suffix: '.min'}))
  }
  return t.pipe(gulp.dest(paths.root + '/js'));
});

gulp.task('jade', function(done) {
  gulp.src(paths.jade)
    .pipe(jade())
    .pipe(gulp.dest(paths.root))
    .on('end', done);
});

gulp.task('sass', function(done) {
  gulp.src('./src/scss/ionic.app.default.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);

  gulp.src('./src/scss/ionic.app.dark.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

gulp.task('watch', function () {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.jade, ['jade'], function () {
    gulp.src(paths.jade).pipe(connect.reload());
  });
  gulp.watch(paths.js_other, ['js_other'], function () {
    gulp.src(paths.js_other).pipe(connect.reload());
  });
  gulp.watch(paths.js_controllers, ['js_controllers'], function () {
    gulp.src(paths.js_controllers).pipe(connect.reload());
  });
});

gulp.task('connect', function () {
  return connect.server({
    root: [ paths.root ],
    livereload: true,
    port:'3000'
  });
});

gulp.task('open', function () {
  return gulp.src('./www/index.html').pipe(open({ uri: 'http://localhost:3000/index.html'}));
});

gulp.task('server', ['default', 'watch', 'connect', 'open']);
