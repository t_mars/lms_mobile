angular.module('addVideoFormProvider', [])
.provider('addVideoForm', function () {
  this.$get = ['$rootScope', '$ionicModal', 'ionicDatePicker', '$ionicLoading', '$http', '$filter', '$ionicSideMenuDelegate', '$interval',
    function ($rootScope, $ionicModal, ionicDatePicker, $ionicLoading, $http, $filter, $ionicSideMenuDelegate, $interval) {

    var provider = {};

    var $scope = $rootScope.$new();

    // форма загрузки
    $ionicModal.fromTemplateUrl('templates/video/add_form.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.formModal = modal;
    });
    $scope.resetVideo = function (params) {
      $scope.video = {
        title: '',
        description: '',
        is_public: true,
        exercise_id: null,
        exercise_name: '-',
        app: 1,
        reps: null,
        weight: null,
        scheme_id: null,
        file: null,
        onsuccess: null,
        executed_at: new Date()
      };
      angular.extend($scope.video, params);
    };
    $scope.resetVideo();

    // Статус загрузки
    $scope.showProgress = function (video_id, onsuccess) {
      var count = 0;
      var config = {
        withCredentials: true
      };
      $rootScope.addUpload({title: 'Обработка видео', progress: 0, id: video_id});
      stop = $interval(function() {
        $http.get(api('video/get_progress/?video_id='+video_id), config)
        .success(function (data) {
          if (data.status == 'process') {
            $rootScope.setUploadStatus(video_id, data.percent);
          } else if (data.status == 'error') {
            $rootScope.showError(data.message);
            $rootScope.delUpload(video_id);
            $interval.cancel(stop);
          } else if (data.status == 'success') {
            if (onsuccess)
              onsuccess(video_id);
            $rootScope.showSuccess('Видео сохранено');
            $rootScope.delUpload(video_id);
            $interval.cancel(stop);
          } else {
            count += 1;
          }

          if (count == 50) {
            $rootScope.showError('Ошибка при обработке видео. Превышено максимальное количество попыток.');
            $interval.cancel(stop);
            $rootScope.delUpload(video_id)
          }
        })
        .error(function () {
          $rootScope.showError('Ошибка №1. Обратитесь к администратору');
          $rootScope.delUpload(video_id)
        })
      }, 500);
    };

    // показать форму добавления видео
    $scope.showForm = function (params) {
      $scope.resetVideo(params);
      $scope.formModal.show();
    };
    // убрать форму добалвения видео
    $scope.closeForm = function () {
      $scope.formModal.hide();
    };

    $scope.input = document.createElement('input');
    $scope.input.setAttribute("type", "file");

    // для внешнего вызова
    provider.showForm = function (params) {
      params = params || {};

      // загрузка из браузера
      if ($rootScope.platform == 'browser' || $rootScope.platform == 'ios') {
        params.mode = 'browser';
        $scope.input.onchange = function () {
          if (!this.files[0].type.startsWith('video/')) {
            $rootScope.showError('Вы выбрали не видео-файл');
            return;
          }
          $scope.showForm(params);
        };
        $scope.input.click();
      } else {
      // Загрузка из галереи
        params.mode = 'plugin';
        navigator.camera.getPicture(onSuccess, onFail, {
          quality: 100,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
          mediaType: Camera.MediaType.VIDEO
        });
        function onSuccess(imageUri) {
          params.file = imageUri;
          $scope.showForm(params);
        }

        function onFail(message) {
          $rootScope.showError('Ошибка при выборе видео: ' + message);
        }
      }
    };

    $scope.successVideoUpload = function (data, id) {
      if (data.status == 'success') {
        if ($scope.video.onsuccess) {
          $scope.video.onsuccess(data.video_id);
        }
        $rootScope.showInfo('Видео сохранено');
      }
      else if (data.status == 'progress') {
        $scope.showProgress(data.video_id, $scope.video.onsuccess);
      }
      else if (data.status == 'error') {
        $rootScope.showInfo(data.message);
      }
      $rootScope.delUpload(id);
    };
    $scope.errorVideoUpload = function (id) {
      $rootScope.delUpload(id);
      $rootScope.showError('Ошибка в процессе загрузки видео.');
    };

    $scope.sendForm = function () {
      var ps = {
        type: 'file',
        title: $scope.video.title,
        description: $scope.video.description,
        app: $scope.video.app
      };

      if ($scope.video.scheme_id) {
        ps.scheme_id = $scope.video.scheme_id;
      } else {
        if (!$scope.video.reps) {
          $rootScope.showError('Укажите количество повторений');
          return;
        }
        if (!$scope.video.weight) {
          $rootScope.showError('Укажите вес снаряда');
          return;
        }
        if (!$scope.video.exercise_id) {
          $rootScope.showError('Укажите упражнение');
          return;
        }
        if (!$scope.video.executed_at) {
          $rootScope.showError('Укажите дату выполнения');
          return;
        }

        ps.weight = $scope.video.weight;
        ps.reps = $scope.video.reps;
        ps.exercise_id = $scope.video.exercise_id;
        ps.executed_at = $filter('date')($scope.video.executed_at, 'dd.MM.yyyy');
      }

      if ($scope.video.mode == 'browser') {
      // загрузка браузером
        var formData = new FormData();
        for (var key in ps) {
          formData.append(key, ps[key]);
        }
        formData.append("file", $scope.input.files[0]);

        var config = {
          withCredentials: true,
          headers: {'Content-Type': undefined},
          transformRequest: angular.identity
        };

        var id = (new Date()).getTime();
        $rootScope.addUpload({title: 'Загрузка видео', id: id, infinity: true});
        $http.post(api('video/save/'), formData, config)
          .success(function (data) {
            $scope.successVideoUpload(data, id);
          }).error(function () {
            $scope.errorVideoUpload(id);
          });

      } else if ($scope.video.mode == 'plugin') {
      // загрузка плагином
        var options = new FileUploadOptions();
        options.fileKey = "file"; //depends on the api
        options.fileName = $scope.video.file.substr($scope.video.file.lastIndexOf('/')+1);
        options.mimeType = "video/" + $scope.video.file.substr($scope.video.file.lastIndexOf('.')+1);
        options.params = ps;
        options.chunkedMode = false; //this is important to send both data and files
        
        var ft = new FileTransfer();
        var id = (new Date()).getTime();
        $rootScope.addUpload({title: 'Загрузка видео', progress: 0, id: id});
        ft.onprogress = function(progressEvent) {
          if (progressEvent.lengthComputable) {
            $rootScope.setUploadStatus(id, parseInt(progressEvent.loaded / progressEvent.total * 100));
          }
        };

        function succesFileTransfer(r) {
          $scope.successVideoUpload(JSON.parse(r.response), id);
        }
        function errorFileTransfer(error) {
          $scope.errorVideoUpload(id);
        }

        ft.upload($scope.video.file, encodeURI(api('video/save/')), succesFileTransfer, errorFileTransfer, options);
      }

      $scope.closeForm();
      $ionicSideMenuDelegate.toggleLeft();
    };

    // выбор упражнения
    $scope.makeSelector = function (params) {
      var scope = params.scope.$new(true);
      scope.data = {};

      scope.data.title = params.title;
      scope.data.query = '';

      params.getItems(scope);

      scope.search = function () {
        if (scope.data.query) {
          scope.data.items = [];
          angular.forEach(
            $filter('smartSearch')(scope.data.all_items, scope.data.query.split(' '), {name: true}),
            function (item) {
              scope.data.items.push(item.entry);
            }
          );
        } else {
          scope.data.items = scope.data.all_items;
        }
      };

      scope.show = function () {
        scope.data.current_id = params.getCurrent();
        scope.data.items = scope.data.all_items;
        scope.search();
        scope.modal.show();
      };
      scope.hide = function () {
        scope.modal.hide();
      };
      scope.select = function (item) {
        params.onselect(item);
        scope.hide();
      };

      $ionicModal.fromTemplateUrl('templates/blocks/select_modal.html', {
        scope: scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        scope.modal = modal;
      });

      return scope;
    };

    $scope.exercise_selector = $scope.makeSelector({
      scope: $scope,
      title: 'Выберите упражнение',
      getCurrent: function () {
        return $scope.video.exercise_id;
      },
      getItems: function (scope) {
        $ionicLoading.show({template: 'Загрузка...'});
        $http.get(api('video/exercise_list/'))
          .success(function (data) {
            if (data.status == 'error') {
              $rootScope.showError(data.exercises);
            } else {
              scope.data.all_items = data.exercises;
            }
          })
          .finally(function () {
            $ionicLoading.hide();
          });
      },
      onselect: function (item) {
        $scope.video.exercise_id = item.id;
        $scope.video.exercise_name = item.name;
      }
    });

    return provider;

  }];
});
