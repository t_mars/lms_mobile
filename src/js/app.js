function api(path) {
  return 'http://api.last-man.org/' + path;
}
function round_to (number, roundto) {
  return roundto * Math.round(number/roundto);
}
function my_round (number) {
  if (number >= 50.0)
    return round_to(number, 2.5)
/*  if (number <= 15.0)
    return round_to(number, 0.1)
*/  return round_to(number, 1)
}
function get_timestamp(date) {
  return Math.round(date.getTime()/1000);
}

angular.module('filters', [])
.filter('my_round', function() {
  return my_round;
})
.filter('inArray', function() {
  return function(array, value) {
      return array.indexOf(value) !== -1;
  };
})
.filter('info_color', function() {
  return function(input) {
    cs = {
      'success': 'green',
      'info': 'blue',
      'danger': 'red',
      'warning': 'yellow',
      'default': '',
    };
    return cs[input];
  };
})
.filter('level_color', function() {
  return function(input) {
    cs = {
      'hard': 'red',
      'light': 'green',
      'middle': 'yellow',
      'other': 'grey',
    };
    return cs[input];
  };
})
.filter('ts2date', function() {
  return function(input) {
    return new Date(input * 1000);
  };
})
.filter('online_status', function ($filter) {
  return function (last_request) {
    if (!last_request) return '<small class="status-offline">офлайн</small>';
    var r = new Date(last_request * 1000),
      rd = new Date(last_request * 1000).setHours(0, 0, 0, 0),
      d = new Date();
    if ((d.getTime() - r.getTime()) < 900000) {
      return '<small class="status-online">онлайн</small>'
    }
    if (d.setHours(0, 0, 0, 0) == rd) {
      return '<small class="status-offline">был ' + $filter('date')(r, 'в HH:mm') + '</small>';
    }
    return '<small class="status-offline">был ' + $filter('date')(r, 'dd.MM в HH:mm') + '</small>';
  }
});

angular.module('starter', ['ionic', 'starter.controllers', 'filters', 'ui.router'])
.run(function($ionicPlatform, $state, storage, $ionicPopup, $ionicLoading) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    ionic.Platform.fullScreen(false, true);

    // если есть запущенная транировка открываем ее
    var currentTrainId = storage.get(['current', 'train', 'id']);
    if (currentTrainId) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'У вас не завершена тренировка',
        template: 'Открыть ее?'
      });

      confirmPopup.then(function(res) {
        if(res) {
          $ionicLoading.show({template: 'Сохранение...'});
          $state.go('app.train_show', {id: currentTrainId}).then(function() {
            $ionicLoading.hide();
          });
        }
      });
    }
  });
})
.config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.backButton.text('');
  //$ionicConfigProvider.backButton.icon('');
  $httpProvider.defaults.withCredentials = true;
  $httpProvider.defaults.xsrfCookieName = 'csrftoken';
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

  var v = '?14.12';
  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html'+v,
      controller: 'AppCtrl'
    })
    .state('app.ws_index', {
      url: '/ws',
      views: {
        'menuContent': {
          templateUrl: 'templates/ws/index.html'+v,
          contoller: 'WsIndexCtrl'
        }
      }
    })
    .state('app.cycle_list', {
      url: '/cycle/list',
      views: {
        'menuContent': {
          templateUrl: 'templates/cycle/list.html'+v,
          controller: 'CycleListCtrl'
        }
      }
    })
    .state('app.cycle_show', {
      cache: false,
      url: '/cycle/:num',
      views: {
        'menuContent': {
          templateUrl: 'templates/cycle/show.html'+v,
          controller: 'CycleShowCtrl'
        }
      }
    })
    .state('app.plan_list', {
      url: '/plan/list',
      views: {
        'menuContent': {
          templateUrl: 'templates/plan/list.html'+v,
          controller: 'PlanListCtrl'
        }
      }
    })
    .state('app.plan_show', {
      url: '/plan/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/plan/show.html'+v,
          controller: 'PlanShowCtrl'
        }
      }
    })
    .state('app.exercise_list', {
      url: '/exercise/list',
      views: {
        'menuContent': {
          templateUrl: 'templates/exercise/list.html'+v,
          controller: 'ExerciseListCtrl'
        }
      }
    })
    .state('app.exercise_show', {
      cache: false,
      url: '/exercise/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/exercise/show.html'+v,
          controller: 'ExerciseShowCtrl'
        }
      }
    })
    .state('app.spm_list', {
      url: '/spm/list',
      views: {
        'menuContent': {
          templateUrl: 'templates/spm/list.html'+v,
          controller: 'SPMListCtrl'
        }
      }
    })
    .state('app.spm_show', {
      url: '/spm/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/spm/show.html'+v,
          controller: 'SPMShowCtrl'
        }
      }
    })
    .state('app.util_converter', {
      url: '/util/converter',
      views: {
        'menuContent': {
          templateUrl: 'templates/util/converter.html'+v,
          controller: 'UtilConverterCtrl'
        }
      }
    })
    .state('app.util_calc', {
      url: '/util/calc',
      views: {
        'menuContent': {
          templateUrl: 'templates/util/calc.html'+v,
          controller: 'UtilCalcCtrl'
        }
      }
    })
    .state('app.util_warmup', {
      url: '/util/warmup',
      views: {
        'menuContent': {
          templateUrl: 'templates/util/warmup.html'+v,
          controller: 'UtilWarmupCtrl'
        }
      }
    })
    .state('app.weight_stat', {
      url: '/weight-stat',
      views: {
        'menuContent': {
          templateUrl: 'templates/weight/stat.html'+v,
          controller: 'WeightStatCtrl'
        }
      }
    })
    .state('app.train_show', {
      cache: false,
      url: '/train/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/train/show.html'+v,
          controller: 'TrainShowCtrl'
        }
      }
    })
    .state('app.video_list', {
      url: '/video/list',
      views: {
        'menuContent': {
          templateUrl: 'templates/video/list.html'+v,
          controller: 'VideoListCtrl'
        }
      }
    })
    .state('app.video_show', {
      cache: false,
      url: '/video/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/video/show.html'+v,
          controller: 'VideoShowCtrl'
        }
      }
    })
    .state('app.payment_list', {
      url: '/payment/list',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/payment/list.html'+v,
          controller: 'PaymentListCtrl'
        }
      }
    })
    .state('app.profile_edit', {
      cache: false,
      url: '/profile/edit',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/edit.html'+v,
          controller: 'ProfileEditCtrl'
        }
      }
    })
    .state('app.user_list', {
      cache: true,
      url: '/user/list',
      views: {
        'menuContent': {
          templateUrl: 'templates/user/list.html'+v,
          controller: 'UserListCtrl'
        }
      }
    })
    .state('app.user_show', {
      cache: true,
      url: '/user/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/user/show.html'+v,
          controller: 'UserShowCtrl'
        }
      }
    })
    .state('app.chat', {
      url: '/chat',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/chat/index.html'+v,
          controller: 'ChatCtrl'
        }
      }
    })
    .state('app.students', {
      url: '/students',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/students.html'+v,
          controller: 'StudentListCtrl'
        }
      }
    })
    .state('app.notifications', {
      url: '/notifications',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/notifications.html'+v,
          controller: 'NotificationListCtrl'
        }
      }
    })
    .state('app.favorites', {
      url: '/favorites',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/favorites.html'+v,
          controller: 'FavoriteListCtrl'
        }
      }
    })
    .state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/app/about.html'+v
        }
      }
    })
    .state('app.settings', {
      url: '/settings',
      views: {
        'menuContent': {
          templateUrl: 'templates/app/settings.html'+v
        }
      }
    })
    .state('app.donate', {
      url: '/donate',
      views: {
        'menuContent': {
          templateUrl: 'templates/app/donate.html'+v,
          controller: 'DonateCtrl'
        }
      }
    })
    .state('app.pay', {
      url: '/pay/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/app/pay.html'+v,
          controller: 'PayCtrl'
        }
      }
    })
    .state('login', {
      url: '/login',
      templateUrl: "templates/login.html"+v,
      controller: 'LoginCtrl'
    })
    ;
  $urlRouterProvider.otherwise('/app/ws');
});
