ctrls.controller('DonateCtrl', function($scope, $rootScope, $http, $ionicLoading, payment) {
  $scope.data = {
    sum: 100,
    comment: ''
  };

  $scope.submit = function () {
    payment.init();
    $ionicLoading.show({template: 'Загрузка'});
    var params = {sum: $scope.data.sum, comment: $scope.data.comment};
    $http.get(api('v2/payment/donate/'), {params: params})
      .success(function (data) {
        if (data.status == 'success') {
          payment.pay(data.id, 'Большое спасибо за пожертвование!<br>Ваши средства пойдут на развитие проекта.');
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Нет сети');
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };
});
