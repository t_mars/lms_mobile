ctrls.controller('GlobalCtrl', function() {});

ctrls.controller('SettingsCtrl', function($scope, $rootScope) {
  $scope.style = $rootScope.style;
  $scope.save = function (style) {
    $rootScope.style = style;
    $rootScope.storage.set(['settings', 'style'], style);
  }
});
