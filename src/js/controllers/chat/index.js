ctrls.controller('ChatCtrl', function($scope, $rootScope, $timeout, $ionicScrollDelegate, $sce, $ionicModal, $filter, $state) {
  var delegate = $ionicScrollDelegate.$getByHandle('chatScroll');

  $scope.send = function (data) {
    if (!$scope.connected)
      return;
    $scope.ws.send(JSON.stringify(data));
  };

  $scope.sendMessage = function() {
    $scope.send({type: 'message', 'content': $scope.data.message});
    $scope.data.message = '';
    $timeout(function() {
      delegate.scrollBottom(true);
    }, 300);
  };

  $scope.update = function () {
    $scope.send({type: 'get_messages', id: $scope.messages[0].id});
  };

  $scope.reset = function () {
    $scope.connected = false;
    $scope.data = {message: '', query: ''};
    $scope.users = [];
    $scope.messages = [];
    $scope.count = 0;
    $scope.guests_count = 0;
  };

  $scope.init = function () {
    $scope.ws = new WebSocket("ws://ws.last-man.org:5000/ws");
    $scope.reset();

    $scope.ws.onopen = function () {
      $scope.connected = true;
      $scope.send({type: 'auth', sessionid: $rootScope.sessionid});
      $scope.$apply();
    };

    $scope.ws.onmessage = function (evt) {
      var data = JSON.parse(evt.data);
      $scope.onmessage(data);
      $scope.$apply();
    };

    $scope.ws.onclose = function () {
      $scope.reset();
      $scope.$apply();
      setTimeout($scope.init, 2000);
    };
  };

  $scope.parseContent = function (content) {
    content = content.replace('href="/user/', 'href="#/app/user/');
    content = content.replace('target="_blank"', '');
    return content;
  };
  $scope.onmessage = function (data) {
    if (data.type == 'load') {
      $scope.messages = [];
      $scope.users = {};
      angular.forEach(data.msgs, function (m, i) {
        m.content = $scope.parseContent(m.content);
        $scope.messages.push(m);
      });
      angular.forEach(data.users, function (u) {
        $scope.users[u.id] = u;
      });
      $timeout(function() {
        delegate.resize();
        delegate.scrollBottom();
      }, 300);
    }
    else if (data.type == 'online') {
      if (data.user) {
        $scope.users[data.user.id] = data.user;
      }
      $scope.count = data.count;
      $scope.guests_count = data.guests_count;
    }
    else if (data.type == 'offline') {
      if (data.user_id) {
        delete $scope.users[data.user_id];
      }
      $scope.count = data.count;
      $scope.guests_count = data.guests_count;
    }
    else if (data.type == 'msg') {
      data.msg.content = $scope.parseContent(data.msg.content);
      $scope.messages.push(data.msg);
      document.getElementById('audio').play();
      $timeout(function() {
        delegate.resize();
        delegate.scrollBottom();
      }, 300);
    }
    else if (data.type == 'alert') {
      $rootScope.showInfo(data.content);
    }
    else if (data.type == 'del') {
      for (var i in data['ids']) {
        var id = data['ids'][i];
        for (var j in $scope.messages) {
          if ($scope.messages[j].id == id) {
            $scope.messages.splice(j, 1);
            break;
          }
        }
      }
      $timeout(function() {
        delegate.resize();
        delegate.scrollBottom();
      }, 300);
    }
    else if (data.type == 'messages') {
      angular.forEach(data.msgs, function (m) {
        m.content = $scope.parseContent(m.content);
        $scope.messages.unshift(m);
      });
      $scope.$broadcast('scroll.refreshComplete');
    }
  };

  $scope.getIcon = function (user) {
    if ($scope.users[user.id]) {
      if ($scope.users[user.id].is_mobile) {
        return 'online-icon ion-iphone';
      }
      if ($scope.users[user.id].is_superuser) {
        return 'online-icon ion-ios-star';
      }
      return 'online-icon ion-person';
    }

    if (user.is_superuser) {
      return 'offline-icon ion-ios-star';
    }
    return 'offline-icon ion-person';
  };
  $scope.profile = function (user) {
    $state.go('app.user_show', {id: user.id});
    $scope.usersModal.hide();
  };

  //список пользователей
  $ionicModal.fromTemplateUrl('templates/chat/users.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.usersModal = modal;
  });

  $scope.search = function () {
    if ($scope.data.query) {
      $scope.user_list = [];
      var ls = $filter('smartSearch')(Object.values($scope.users), $scope.data.query.split(' '), {username: true, name: true});
      for (var i in ls) {
        $scope.user_list.push(ls[i].entry);
      }
    } else {
      $scope.user_list = $scope.users;
    }
  };

  $scope.addRecipient = function (username) {
    if ($scope.data.message != '' && $scope.data.message[$scope.data.message.length-1] != ' ') {
      $scope.data.message += ' ';
    }
    $scope.data.message += '@' + username + ' ';
    $timeout(function () {
      document.getElementById('message').focus();
    }, 100);
  };

  $scope.init();
});
