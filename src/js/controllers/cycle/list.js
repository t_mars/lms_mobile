ctrls.controller('CycleListCtrl', function($scope, $rootScope, $http, $filter, $ionicLoading) {
  $scope.data = {
    query: '',
    all_cycles: [],
    cycles: []
  };

  var options = {
    sport: true,
    mode: true,
    name: true,
    level: true
  };

  $scope.refresh = function() {
    $ionicLoading.show({template: 'Загрузка'});
    $http.get(api('cycle/list/'))
      .success(function (data) {
        $scope.data.all_cycles = data.list;
        $scope.data.cycles = data.list;
      })
      .error(function () {
        $rootScope.showError('Ошибка сети');
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  $scope.search = function () {
    if ($scope.data.query) {
      $scope.data.cycles = [];
      angular.forEach(
        $filter('smartSearch')($scope.data.all_cycles, $scope.data.query.split(' '), options),
        function (item) {
          $scope.data.cycles.push(item.entry);
        }
      );
    } else {
      $scope.data.cycles = $scope.data.all_cycles;
    }
  };

  $scope.refresh();
});
