ctrls.controller('CycleShowCtrl', function($scope, $rootScope, $http, $stateParams, $filter, $ionicModal, $ionicPopup, $state, $ionicLoading) {
  $scope.plan = {
    name: '',
    correct_percent: 0.5,
  };
  $scope.date_start = {
    'show': new Date(),
    'value': new Date(),
  };

  $ionicLoading.show({template: 'Загрузка'});
  $http.get(api('cycle/'+$stateParams.num+'/'))
    .success(function(data) {
      if (data.status == 'success') {
        $scope.cycle = data.data;
        $scope.week_days = {};
        $scope.plan.name = data.data.name;
        for (var i = 0; i < 7; i++) {
          $scope.week_days[i] = $filter('inArray')($scope.cycle.week_days, i);
        }
        angular.forEach($scope.cycle.exercises, function(e, eid) {
          $scope.cycle.weights[eid] = my_round($scope.cycle.weights[e.parent_id] * e.parent_rate);
        });

        $scope.reset();
        $scope.resetDates();
      } else if (data.message) {
        $state.go('app.cycle_list');
        $ionicPopup.alert({
          title: 'Ошибка',
          template: data.message
        });
      }
    })
    .finally(function() {
      $ionicLoading.hide();
    });

  $scope.setWeight = function (obj) {
    var obj = this;
    angular.forEach($scope.cycle.exercises, function(e, eid) {
      if (e.parent_id == obj.e.id) {
        $scope.cycle.weights[eid] = my_round($scope.cycle.weights[e.parent_id] * e.parent_rate);
      }
    });
    $scope.reset();
  };

  $scope.reset = function (val) {
    var percent = 1;
    var correct = 1 + $scope.plan.correct_percent / 100;
    angular.forEach($scope.cycle.plan, function(days, micro_num) {
      angular.forEach(days, function(day) {
        angular.forEach(day.list, function(execution) {
          execution.pm = $scope.cycle.weights[execution.exercise.id] * percent;
          angular.forEach(execution.scheme, function(scheme) {
            scheme.weight = execution.pm * scheme.percent / 100;
          })
        })
      });
      percent *= correct;
    })
  };

  $scope.resetDates = function () {
    var get_week_days_set = function (week_days) {
      var res = [];
      angular.forEach(week_days, function(flag, weekday) {
        if (flag) {
          res.push(weekday);
        }
      });
      return res;
    };

    // дата начала
    var d = $scope.date_start.show;
    var day = d.getDay();
    var diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
    $scope.date_start.value = new Date(d.setDate(diff));

    // проверяем правильность дней недели
    var set = get_week_days_set($scope.week_days);
    // если неверно, дни недели по умолчанию
    if (set.length < $scope.cycle.week_days.length) {
      set = $scope.cycle.week_days;
      $scope.date_error = true;
    } else {
      $scope.date_error = false;
    }
    $scope.week_days_set = set;

    angular.forEach($scope.cycle.plan, function(days, week_num) {
      angular.forEach(days, function(day) {
        day.date = angular.copy($scope.date_start.value);
        day.date.setDate($scope.date_start.value.getDate() + week_num*7 + parseInt($scope.week_days_set[day.day_ind]));
      })
    })

  };

  // просмотр по неделям
  $ionicModal.fromTemplateUrl('templates/cycle/week.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.showModal = function (micro_num) {
    $scope.current_week_num = parseInt(micro_num) + 1;
    $scope.current_week = $scope.cycle.plan[micro_num];
    $scope.modal.show();
  };

  $scope.closeModal = function () {
    $scope.modal.hide();
  };

  // создание плана
  $scope.createPlan = function () {
    $ionicLoading.show({
      template: 'Загрузка...'
    });
    data = {
      'weights': $scope.cycle.weights,
      'correct_percent': $scope.plan.correct_percent,
      'plan_name': $scope.plan.name,
      'week_days': $scope.week_days_set,
      'start_date': $filter('date')($scope.date_start.value, 'dd.MM.yy'),
    };
    $http.post(
      api('cycle/'+$stateParams.num+'/create-plan/?&csrfmiddlewaretoken='+$scope.cycle.csrf),
      data
    )
    .success(function(data) {
      $ionicLoading.hide();
      if (data.status == 'error') {
        if (data.code == 'auth_error') {
          $ionicPopup.alert({
            title: 'Ошибка',
            template: 'Для создания плана необходимо авторизироваться.'
          });
        } else {
          $ionicPopup.alert({
            title: 'Ошибка',
            template: data.message
          });
        }
      } else if (data.status == 'success') {
        $state.go('app.plan_show', {id: data.id});
      }
    })
    .error(function() {
      $rootScope.showError('Ошибка передачи данных.');
    })
    .finally(function() {
      $ionicLoading.hide();
    });
  };

});
