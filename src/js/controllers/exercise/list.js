ctrls.controller('ExerciseListCtrl', function($scope, $rootScope, $http) {
  $scope.update = $rootScope.updateData({
    getKeys: function () {return ['exercise_list']},
    resetData: function () {
      $scope.exercises = [];
      $scope.groups = [];
    },
    getData: function (data) {
      return data;
    },
    setData: function (data) {
      $scope.exercises = data.list;
      $scope.groups = data.groups;
    },
    request: function () {return $http.get(api('exercise/list/'))}
  });

  $scope.update();
});
