ctrls.controller('ExerciseShowCtrl', function($scope, $rootScope, $http, $stateParams, $sce) {
  $scope.update = $rootScope.updateData({
    getKeys: function () {return ['exercise', $stateParams.id]},
    resetData: function () {
      $scope.exercise = null;
    },
    getData: function (data) {
      return data;
    },
    setData: function (data) {
      $scope.exercise = data;
      $scope.exercise.content = $sce.trustAsHtml(data.content);
    },
    request: function () {return $http.get(api('exercise/'+$stateParams.id))}
  });

  $scope.update();
});
