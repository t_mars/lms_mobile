ctrls = angular.module('starter.controllers',
  ['ionic', 'ionic-timepicker', 'ionic-datepicker', 'ngSmartSearch', 'addVideoFormProvider', 'userSelectProvider', 'ngCordova']);
document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    window.open = cordova.InAppBrowser.open;
}
ctrls.run(['$rootScope', 'config', '$timeout', 'storage', '$ionicLoading', '$http', '$ionicPopup', '$state',
  function ($rootScope, config, $timeout, storage, $ionicLoading, $http, $ionicPopup, $state) {
  $rootScope.version = config.version;
  $rootScope.platform = config.platform;

  // Работа с хранилищем
  $rootScope.storage = storage;

  // Вывод сообщений
  $rootScope.showMessage = function (message, cls) {
    $rootScope.message = message;
    $rootScope.message_class = cls;
    $timeout(function() {
       $rootScope.message = '';
    }, 5000);
  };
  $rootScope.showError = function (message) {
    $rootScope.showMessage(message, 'error');
  };
  $rootScope.showSuccess = function (message) {
    $rootScope.showMessage(message, 'success');
  };
  $rootScope.showInfo = function (message) {
    $rootScope.showMessage(message, 'info');
  };
  $rootScope.showMessagePopup = function (title, content) {
    $ionicPopup.alert({title: title, template: content});
  };

  // Работа с данными кеширования
  $rootScope.updateData = function (params) {
    return function (refresh) {
      if (params.getKeys) {
        params.keys = params.getKeys();
      }

      if (!refresh) {
        var data = $rootScope.storage.get(params.keys, true);
        if (data) {
          params.setData(data);
          return;
        }
      }

      if (refresh == 'hidden_refresh') {
        var data = $rootScope.storage.get(params.keys, true);
        if (data) {
          params.setData(data);
        }
      } else {
        $ionicLoading.show({template: 'Загрузка...'});
      }

      params.request()
        .success(function (data) {
          if (data.status == 'error') {
            if (data.type == 'auth_error') {
              $rootScope.logout();
            }
            $rootScope.showError(data.message);
            params.resetData();
          } else {
            var d = params.getData(data);
            $rootScope.storage.set(params.keys, d);
            params.setData(d);
          }
        })
        .error(function () {
          var data = $rootScope.storage.get(params.keys);
          if (data) {
            params.setData(data);
            if (refresh != 'hidden_refresh') {
              $rootScope.showInfo('Ошибка сети: загружено из кеша.');
            }
          } else {
            params.resetData();
            if (refresh != 'hidden_refresh') {
              $rootScope.showError('Ошибка сети.');
            }
          }
        })
        .finally(function() {
          if (refresh != 'hidden_refresh') {
            $ionicLoading.hide();
            $rootScope.$broadcast('scroll.refreshComplete');
          }
        });
    };
  };

  // работа с видео
  $rootScope.uploads = {};
  // добавление
  $rootScope.addUpload = function (args) {
    $rootScope.uploads[args.id] = {
      'id': args.id,
      'title': args.title,
      'progress': args.progress,
      'infinity': args.infinity
    };
    $rootScope.$apply();
  };
  // удаление
  $rootScope.delUpload = function (id) {
    if (!$rootScope.uploads[id]) return;
    delete $rootScope.uploads[id];
    $rootScope.$apply();
  };
  // установка статуса
  $rootScope.setUploadStatus = function (id, progress) {
    if (!$rootScope.uploads[id]) return;
    $rootScope.uploads[id].progress = progress;
    $rootScope.$apply();
  };

  // работа с пользователями
  $rootScope.logout = function () {
    $rootScope.storage.clear();
    $state.go('login', {}, {reload: true});
  };

  $rootScope.disconnect = function () {
    if ($rootScope.platform == 'browser') {
      $ionicLoading.show({template: 'Выход...'});
      $http.get(api('auth/disconnect/'))
        .finally(function () {
          $rootScope.logout();
          $ionicLoading.hide();
        });
    } else {
      $ionicLoading.show({template: 'Выход...'});
      var win = window.open('http://ws.last-man.org/logout/', '_blank', 'location=no,clearsessioncache=yes,clearcache=yes,hidden=yes');
      win.addEventListener('exit', function (event) {
        $rootScope.logout();
        $ionicLoading.hide();
        delete win;
      });
      win.addEventListener('loadstop', function (event) {
        win.close();
      });
    }


    $rootScope.logout();
  };

  // проверка авторизации
  $rootScope.checkAuth = function () {
    $rootScope.user = $rootScope.storage.get(['user']);
    $rootScope.sessionid = $rootScope.storage.get(['sessionid']);
    $rootScope.csrftoken = $rootScope.storage.get(['csrftoken']);

    $http.get(api('v2/auth/check/?platform='+$rootScope.platform+'&version='+$rootScope.version))
      .success(function(data) {
        if (data.status == 'success') {
          $rootScope.storage.set(['user'], data.user);
          $rootScope.storage.set(['sessionid'], data.sessionid);
          $rootScope.storage.set(['csrftoken'], data.csrftoken);
          $rootScope.user = data.user;
          $rootScope.sessionid = data.sessionid;
          $rootScope.csrftoken = data.csrftoken;
        } else {
          $rootScope.logout();
        }
      })
      .error(function () {
        $rootScope.showError('Нет сети');
      })
  };
}]);
ctrls.controller('AppCtrl', function($scope, $rootScope, $ionicPopup, $filter, $ionicLoading, $ionicSideMenuDelegate, $http, $state, $sce) {
  // установка темы
  var style = $rootScope.storage.get(['settings', 'style']);
  $rootScope.style = style ? style : 'dark';

  // обновление при открытии меню
  $scope.msg_content = null;
  $scope.msg_update = null;
  $rootScope.counts_update = null;
  var data = $rootScope.storage.get(['user', 'counts']);
  if (data) {
    $rootScope.counts = data;
  } else {
    $rootScope.counts = {
      favorites: 0,
      students: 0,
      notifications: 0
    };
  }

  $rootScope.resetUserCounts = function () {
    var d = new Date();
    $http.get(api('v2/profile/counts/'))
      .success(function (data) {
        if (data.status == 'success') {
          $rootScope.storage.set(['user', 'counts'], data.counts);
          $rootScope.counts_update = d;
        }
      })
      .finally(function () {
        var data = $rootScope.storage.get(['user', 'counts']);
        if (data) {
          $rootScope.counts = data;
        }
      });
  };

  $scope.$watch(function () {
    return $ionicSideMenuDelegate.getOpenRatio();
  },
  function (ratio) {
    if (ratio != 1) return;

    var d = new Date();
    if ($rootScope.date.setHours(0,0,0,0) != d.setHours(0,0,0,0)) {
      $rootScope.date = d;
      $scope.resetCounts();
    }

    // сообщение с сервера
    if (!$scope.msg_update || (d.getTime() - $scope.msg_update.getTime()) > 300000) { // 5 минут
      $http.get(api('v2/ws/message/'))
        .success(function (data) {
          if (data.status == 'success') {
            $scope.msg_content = $sce.trustAsHtml(data.content);
            $scope.msg_update = d;
          }
        });
    }

    if (!$rootScope.counts_update || (d.getTime() - $rootScope.counts_update.getTime()) > 300000) { // 5 минут
      $rootScope.resetUserCounts();
    }
  });

  // шаринг тренировок
  $rootScope.shareTrain = function (train) {
    var train_content = 'Тренировка: ' + train.name + '\n';
    for (var i in train.executions) {
      var e = train.executions[i];
      train_content += (parseInt(i)+1)+'. ';
      if (e.level == 'other') {
        train_content += e.other.name + ' ';
        train_content += e.other.weight + (e.other.reps ? ' ' + e.other.reps : '') + (e.other.apps ? ' ' + e.other.apps : '');
      } else {
        train_content += e.exercise.name + ':\n';
        for (var j in e.scheme) {
          var s = e.scheme[j];
          train_content += s.weight + 'кг ' + s.apps + ' по ' + s.reps + '\n';
        }
      }
    }
    if (train.comment) {
      train_content += 'Комментарий: ' + train.comment;
    }
    if (train.resume) {
      train_content += 'Итоги: ' + train.resume;
    }

    function share(obj) {
      window.plugins.socialsharing.shareWithOptions(
        obj,
        function (result) {},
        function (msg) {}
      );
    }

    $ionicLoading.show({template: 'Отрисовка...'});
    domtoimage.toBlob(document.getElementById('train_content_'+train.id))
      .then(function (blob) {
        $ionicLoading.hide();
        var folderpath = null;
        if (window.cordova) {
          if (cordova.file.externalRootDirectory) { // android
            folderpath = cordova.file.externalRootDirectory;
          } else { // ios
            folderpath = cordova.file.tempDirectory;
          }
        }

        if (folderpath) {
          var filename = "train-" + train.id + ".png";
          window.resolveLocalFileSystemURL(folderpath, function (dir) {
            dir.getFile(filename, {create: true}, function (file) {
              file.createWriter(function (fileWriter) {
                fileWriter.write(blob);
                share({message: train.name, files: [folderpath + '/' + filename]});
              }, function () {
                $rootScope.showError('Ошибка при сохранении картинки');
                share({message: train_content});
              });
            });
          });
        } else {
          share({message: train_content});
          $rootScope.showError('Ошибка файловой системы');
        }
      })
      .catch(function (error) {
        $ionicLoading.hide();
        $rootScope.showError('Ошибка при построении изображения');
        share({message: train_content});
      });
  };

  // запрос счетчиков
  $rootScope.setCounts = function (data) {
    if (data) {
      $rootScope.storage.set(['ws_resume'], data);
    } else {
      data = $rootScope.storage.get(['ws_resume']);
    }

    if (data.trains_count > 0) {
      if (data.trains_done > 0) {
        $scope.trains_badge = data.trains_done + '/' + data.trains_count;
      } else {
        $scope.trains_badge = data.trains_count;
       }
    } else {
      $scope.trains_badge = 0;
    }

    if (data.meds_count > 0) {
      if (data.meds_done > 0) {
        $scope.meds_badge = data.meds_done + '/' + data.meds_count;
      } else {
        $scope.meds_badge = data.meds_count;
      }
    } else {
      $scope.meds_badge = 0;
    }

    // if (window.cordova && cordova.plugins && cordova.plugins.notification) {
    //   cordova.plugins.notification.badge.set((data.trains_count-data.trains_done) + (data.meds_count-data.meds_done));
    // }
  };

  $scope.resetCounts = function () {
    $http.get(api('v2/ws/resume/?date='+$filter('date')($rootScope.date, 'dd.MM.yy')))
      .success(function(data) {
        $rootScope.setCounts(data);
      })
      .error(function () {
        $rootScope.showError('Нет сети2');
        $rootScope.setCounts();
      })
  };

  //инициализация
  $rootScope.date = new Date();
  $rootScope.checkAuth();
});
