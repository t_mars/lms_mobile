ctrls.controller('LoginCtrl', function($scope, $http, $rootScope, $state, $ionicLoading, $timeout, $ionicHistory) {
  // если страница первая, то проверяем авторизацию
  if ($rootScope.storage.get(['user'])) $state.go('app.ws_index');

  function openApp() {
    $ionicHistory.clearCache().then(function () {
      $state.go('app.ws_index').then(function () {
        $ionicLoading.hide();
      });
    });
  }

  function loginBrowser(url) {
    var win = window.open(url, '_blank', 'location=no');
    $ionicLoading.show({template: 'Авторизация'});

    function callback() {
      $http.get(api('/v2/auth/check/'))
        .success(function(data) {
          if (data.status == 'success') {
            win.close();
            openApp();
          }
        })
        .error(function () {
          win.close();
          $rootScope.showError('Ошибка сети.');
        })
        .finally(function () {
          if (win.window) {
            $timeout(callback, 1000);
          } else {
            delete win;
          }
        });
    };
    $timeout(callback, 2000);
  }

  function loginCordova(url) {
    var win = window.open(url, '_blank', 'location=no');
    $ionicLoading.show({template: 'Авторизация'});

    win.addEventListener('exit', function () {
      $ionicLoading.hide();
      delete win;
    });
    win.addEventListener('loadstart', function (event) {
      if (event.url.startsWith(api('auth/done/'))) {
        openApp();
        win.close();
      }
    })
  }

  $scope.loginWith = function (mode) {
    var url = api('auth/login/'+mode+'/');
    if ($rootScope.platform == 'browser') {
      loginBrowser(url);
    } else {
      loginCordova(url);
    }
  };
});
