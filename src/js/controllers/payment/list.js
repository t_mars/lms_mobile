ctrls.controller('PaymentListCtrl', function($scope, $rootScope, $http, $ionicLoading, payment) {
  $scope.data = {
    months: 1
  };

  $scope.refresh = function () {
    $ionicLoading.show({template: 'Загрузка'});
    $http.get(api('v2/payment/list/'))
      .success(function(data) {
        if (data.status == 'success') {
          $scope.invoices = data.invoices;
          $scope.taxes = data.taxes;
          $scope.data.months = data.taxes[0].months;
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Нет сети');
      })
      .finally(function() {
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $rootScope.checkAuth();
      });
  };
  $scope.refresh();

  $scope.submit = function () {
    payment.init();
    $ionicLoading.show({template: 'Загрузка'});
    var params = {m: $scope.data.months};
    $http.get(api('v2/payment/subscribe/'), {params: params})
      .success(function (data) {
        if (data.status == 'success') {
          payment.pay(data.id, 'Оплата принята. Подписка активирована');
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Нет сети');
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };
});
