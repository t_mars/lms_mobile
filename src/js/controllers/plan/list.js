ctrls.controller('PlanListCtrl', function($scope, $rootScope, $http) {
  $scope.update = $rootScope.updateData({
    keys: ['profile', 'plans'],
    resetData: function () {$scope.plans = []},
    getData: function (data) {return data.list},
    setData: function (data) {$scope.plans = data},
    request: function () {return $http.get(api('plan/list/'))}
  });

  $scope.update();
});
