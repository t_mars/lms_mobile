ctrls.controller('PlanShowCtrl', function($scope, $rootScope, $http, $stateParams) {
  $scope.start = 0;
  $scope.end = 0;
  $scope.days = [];
  ts = get_timestamp(new Date());

  $http.get(api('plan/'+$stateParams.id+'/'+ts)).success(function(data) {
    $scope.plan = data.plan;
    $scope.start = data.start;
    $scope.end = data.end;
    $scope.days = data.days;
  });

  // обновляем данные тренировки на случай обновления
  $rootScope.$on('storage_set', function(event, keys, data) {
    if (keys[0] != 'train') return;
    angular.forEach($scope.days, function(day, i1) {
      angular.forEach($scope.days[i1].trains, function(train, i2) {
        if (train.id == keys[1]) {
          $scope.days[i1].trains[i2] = data;
        }
      });
    });
  });

  $scope.loadDown = function () {
    if (!$scope.end) {
      $scope.$broadcast('scroll.infiniteScrollComplete');
      return;
    }
    $http
      .get(api('plan/'+$stateParams.id+'/down/'+$scope.end))
      .success(function(data) {
        if (data.days) {
          $scope.days = $scope.days.concat(data.days);
        }
        $scope.end = data.end;
      })
      .finally(function() {
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
  };

  $scope.loadUp = function () {
    if (!$scope.start) {
      $scope.$broadcast('scroll.refreshComplete');
      return;
    }
    $http
      .get(api('plan/'+$stateParams.id+'/up/'+$scope.start))
      .success(function(data) {
        if (data.days) {
          $scope.days = data.days.concat($scope.days);
        }
        $scope.start = data.start;
      })
      .finally(function() {
        $scope.$broadcast('scroll.refreshComplete');
      });
  };
});
