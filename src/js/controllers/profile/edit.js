ctrls.controller('ProfileEditCtrl', function($scope, $rootScope, $http, $ionicLoading, $cordovaFileTransfer, $ionicModal) {
  $scope.update = function () {
    $ionicLoading.show({template: 'Загрузка...'});
    $http.get(api('v2/profile/'))
      .success(function(data) {
        if (data.status == 'success') {
          $scope.user = data.user;
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .finally(function () {
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
      });
  };
  $scope.update();

  $scope.saveProfile = function () {
    $ionicLoading.show({template: 'Сохранение...'});
    $scope.errors = {};

    var data = {
      username: $scope.user.username,
      first_name: $scope.user.first_name,
      last_name: $scope.user.last_name,
      gender: $scope.user.gender,
      allpowerlifting: $scope.user.allpowerlifting,
      trainer_id: $scope.user.trainer ? $scope.user.trainer.id : null
    };
    var url = api('v2/profile/edit/');

    function onSuccess(data) {
      $ionicLoading.hide();
      if (data.status == 'success') {
        $scope.user = data.user;
        $rootScope.user = data.user;
        $rootScope.storage.set(['user'], data.user);

        $rootScope.showSuccess('Изменения сохранены');
      } else if (data.status == 'error') {
        if (data.type == 'auth_error') {
          $rootScope.logout();
        }
        if (data.fields) {
          $scope.errors = data.fields;
          $scope.$apply();
        }
        $rootScope.showError(data.message);
      }
    }

    if ($scope.user.set_avatar) {
      var options = {
        fileKey: "avatar",
        fileName: $scope.user.avatar.substr($scope.user.avatar.lastIndexOf('/') + 1),
        chunkedMode: true,
        mimeType: "image/jpg",
        params: data
      };

      $cordovaFileTransfer.upload(url, $scope.user.avatar, options)
        .then(function (result) {
          var data = JSON.parse(result.response);
          onSuccess(data)
        }, function (error) {
          console.log('error', error, JSON.stringify(error));
          $ionicLoading.hide();
          $rootScope.showError('Ошибка передачи данных');
        });
    } else {
      $http.post(url, data)
        .success(onSuccess)
        .error(function () {
          $ionicLoading.hide();
          $rootScope.showError('Ошибка передачи данных');
        });
    }
  };

  $scope.setAvatar = function () {
    if ($rootScope.platform == 'browser') {
      $rootScope.showError('Загрузить можно только с помощью приложения');
      return;
    }
    if ($rootScope.platform == 'ios') {
      $rootScope.showError('Выбор фото временно недоступен. Будет исправлено в новой версии.');
      return;
    }

    function onSuccess(imageUri) {
      plugins.crop.promise(imageUri, {quality: 100})
        .then(function success (newPath) {
          $scope.user.set_avatar = true;
          $scope.user.avatar = newPath;
          $scope.user.avatar_100 = newPath;
          $scope.$apply();
        })
        .catch(function fail (err) {
          $rootScope.showError('Ошибка обрезки фото');
        });
    }

    function onFail(message) {
      $rootScope.showError('Ошибка при выборе фото: ' + message);
    }

    try {
      navigator.camera.getPicture(onSuccess, onFail, {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        mediaType: Camera.MediaType.PICTURE,
        encodingType: Camera.EncodingType.JPEG
      });

    } catch(e) {
      $scope.msg = ('Ошибка ' + e.name + ":" + e.message + "\n" + e.stack);
    }
  };

  // указание тренера
  $scope.data = {};
  $scope.data.query = '';

  $ionicModal.fromTemplateUrl('templates/profile/user_list.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.modal = modal;
  });

  $scope.showSearch = function () {
    $scope.searchTrainer();
    $scope.modal.show();
  };
  $scope.hideSearch = function () {
    $scope.modal.hide();
  };
  $scope.setTrainer = function (user) {
    $scope.user.trainer = user;
    $scope.hideSearch();
  };

  $scope.searchTrainer = function () {
    $scope.data.search_loading = true;
    $http.get(api('v2/user/search/?q='+$scope.data.query))
      .success(function (data) {
        $scope.user_list = data.list;
      })
      .error(function () {
        $rootScope.showError('Ошибка сети при поиске пользователя.');
      })
      .finally(function () {
        $scope.data.search_loading = false;
      });
  };
});
