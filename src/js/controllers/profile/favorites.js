ctrls.controller('FavoriteListCtrl', function($rootScope, $scope, $http) {
  $scope.update = $rootScope.updateData({
    getKeys: function () {
      return ['user', 'favorites']
    },
    resetData: function () {
      $scope.favorites = [];
    },
    getData: function (data) {
      return data;
    },
    setData: function (data) {
      $scope.favorites = data.list;
    },
    request: function () {
      return $http.get(api('v2/profile/favorites/'))
    }
  });
  $scope.update('hidden_refresh');
});
