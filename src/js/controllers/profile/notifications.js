ctrls.controller('NotificationListCtrl', function($rootScope, $scope, $http, $sce) {
  $scope.update = $rootScope.updateData({
    getKeys: function () {
      return ['user', 'notifications']
    },
    resetData: function () {
      $scope.notifications = [];
    },
    getData: function (data) {
      return data;
    },
    setData: function (data) {
      $scope.notifications = [];
      angular.forEach(data.list, function (n) {
        n.content = $sce.trustAsHtml(n.content);
        $scope.notifications.push(n);
      });
      $rootScope.resetUserCounts();
    },
    request: function () {
      return $http.get(api('v2/profile/notifications/'))
    }
  });
  if ($rootScope.counts.notifications > 0) {
      $scope.update(true);
  } else {
      $scope.update('hidden_refresh');
  }
});
