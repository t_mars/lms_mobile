ctrls.controller('StudentListCtrl', function($rootScope, $scope, $http) {
  $scope.update = $rootScope.updateData({
    getKeys: function () {
      return ['user', 'students']
    },
    resetData: function () {
      $scope.students = [];
    },
    getData: function (data) {
      return data;
    },
    setData: function (data) {
      $scope.students = data.list;
    },
    request: function () {
      return $http.get(api('v2/profile/students/'))
    }
  });
  $scope.update('hidden_refresh');
});
