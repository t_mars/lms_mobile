ctrls.factory('config', function () {
  version = '0.99';
  if (window.cordova) {
    if (ionic.Platform.isIOS() || ionic.Platform.isIPad()) {
      platform = 'ios';
    } else if (ionic.Platform.isAndroid()) {
      platform = 'android';
    } else {
      platform = 'noname';
    }
  } else {
    platform = 'browser';
  }
  return {
    version: version,
    platform: platform
  }
});
