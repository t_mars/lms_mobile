ctrls.factory('payment', ['$rootScope', '$http', '$timeout', function ($rootScope, $http, $timeout) {
  var service = {};

  function payBrowser(pid, message) {
    service.win.location = api('v2/payment/pay/'+pid+'/');
    function callback() {
      $http.get(api('v2/payment/is-done/'+pid+'/'))
        .success(function (data) {
          if (data.status == 'success') {
            if (data.result) {
              service.win.close();
              $rootScope.showSuccess(message);
            }
          } else if (data.status == 'error') {
            service.win.close();
            if (data.type == 'auth_error') {
              $rootScope.logout();
            }
            $rootScope.showError(data.message);
          }
        })
        .error(function () {
          service.win.close();
          $rootScope.showError('Ошибка сети.');
        })
        .finally(function () {
          if (service.win.window) {
            $timeout(callback, 1000);
          }
        });
    };
    $timeout(callback, 2000);
  };

  function payCordova(pid, message) {
    service.win = window.open(api('v2/payment/pay/'+pid+'/'), '_blank', 'location=no');

    service.win.addEventListener('exit', function () {
      delete service.win;

      // проверка оплаты
      $ionicLoading.show({template: 'Проверка оплаты'});
      $http.get(api('v2/payment/is-done/'+pid+'/'))
        .success(function (data) {
          if (data.status == 'success') {
            if (data.result) {
              $rootScope.showSuccess(message);
            } else {
              $rootScope.showError('Оплата не поступила');
            }
          } else if (data.status == 'error') {
            if (data.type == 'auth_error') {
              $rootScope.logout();
            }
            $rootScope.showError(data.message);
          }
        })
        .error(function () {
          $rootScope.showError('Ошибка сети.');
        })
        .finally(function () {
          $ionicLoading.hide();
        });
    });
    service.win.addEventListener('loadstart', function (event) {
      if (event.url.startsWith('https://auth.robokassa.ru/Merchant/State/Done') ||
          event.url.startsWith('https://money.yandex.ru/cashdesk/2.0/success') ||
          event.url.startsWith('http://ws.last-man.org/')) {
        win.close();
      }
    })
  };

  service.init = function () {
    service.win = window.open(null, '_blank', 'location=no');
  };

  service.pay = function (pid, message) {
    if ($rootScope.platform == 'browser') {
      payBrowser(pid, message);
    } else {
      payCordova(pid, message);
    }
  };

  return service;
}]);
