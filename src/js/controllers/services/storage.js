ctrls.factory('storage', ['config', '$rootScope', function (config, $rootScope) {
  return {
    get: function (keys, fresh) {
      var d = window.localStorage.getItem(this.getKey(keys));
      if (!d) {
        return null
      }
      var data = JSON.parse(d);

      if (!fresh) {
        return data['data'];
      }

      var t = parseInt((new Date()).getTime() / 1000);
      if (t < data['js_timeout']) {
        return data['data'];
      }

      return null;
    },
    set: function (keys, data, timeout) {
      timeout = timeout ? timeout : 24*60*60;
      d = {
        'js_timeout': parseInt((new Date()).getTime() / 1000) + timeout,
        'data': data
      };
      $rootScope.$emit('storage_set', keys, data);
      return window.localStorage.setItem(this.getKey(keys), JSON.stringify(d));
    },
    remove: function (keys) {
      return window.localStorage.removeItem(this.getKey(keys));
    },
    clear: function () {
      window.localStorage.clear();
    },
    getKey: function (keys) {
      return keys.join('_') + '_' + config.version;
    }
  }
}]);
