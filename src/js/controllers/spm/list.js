ctrls.controller('SPMListCtrl', function($scope, $rootScope, $http, $state, $filter, $ionicModal, $ionicLoading) {
  $scope.update = $rootScope.updateData({
    keys: ['profile', 'spms'],
    resetData: function () {$scope.spms = []},
    getData: function (data) {return data.list},
    setData: function (data) {$scope.spms = data},
    request: function () {return $http.get(api('v2/course/list/'))}
  });
  $scope.$on('$ionicView.enter', function() {
    $scope.update('hidden_refresh');
  });

  $ionicModal.fromTemplateUrl('templates/spm/create.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.formModal = modal;
  });

  $scope.showForm = function () {
    $scope.course = {
      'name': '',
      'start_date': new Date(),
      'is_public': true
    };

    $scope.formModal.show();
  };

  $scope.hideForm = function () {
    $scope.formModal.hide();
  };

  $scope.createCourse = function () {
    var data = {
      'name': $scope.course.name,
      'start_date': $filter('date')($scope.course.start_date, 'dd.MM.yy'),
      'is_public': $scope.course.is_public
    };
    var config = {
      headers: {
        'X-CSRFToken': $rootScope.csrftoken
      }
    };
    $ionicLoading.show({template: 'Сохранение'});
    $http.post(api('v2/course/create/'), data, config)
      .success(function(data) {
        if (data.status == 'success') {
          $scope.formModal.hide();
          $state.go('app.spm_show', {id: data.course.id});
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Ошибка сети.');
      })
      .finally(function() {
        $ionicLoading.hide();
      });
  };
});
