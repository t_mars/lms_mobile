ctrls.controller('SPMShowCtrl', function($scope, $rootScope, $http, $stateParams, $filter, $ionicLoading, $ionicSideMenuDelegate,
                                         $ionicModal, $ionicPopup, $ionicScrollDelegate, $timeout, userSelect, $ionicHistory) {

  // блокировка открытия side-menu перетаскиванием
  $scope.$on('$ionicView.enter', function() {
    $ionicSideMenuDelegate.canDragContent(false);
  });
  $scope.$on('$ionicView.leave', function() {
    $ionicSideMenuDelegate.canDragContent(true);
  });

  $scope.mouse = {};
  $scope.mouse.down = null;
  $scope.mouse.shift = null;
  $scope.mouse.percent = 0.5;

  function mousemove (event) {
    if (!$scope.mouse.down) return;
    if (event.type == 'touchmove') {
      pos = {x: event.touches[0].pageX, y: event.touches[0].pageY};
    } else {
      pos = {x: event.pageX, y: event.pageY};
    }
    if ($scope.orient == 'horizontal') {
      $scope.resize(true, pos.x-$scope.mouse.shift);
    }
    else if ($scope.orient == 'vertical') {
      $scope.resize(true, pos.y-$scope.mouse.shift);
    }
  }
  function mousedown (event) {
    var p = 10;
    if (event.type == 'touchstart') {
      pos = {x: event.touches[0].pageX, y: event.touches[0].pageY};
    } else {
      pos = {x: event.pageX, y: event.pageY};
    }
    if ($scope.orient == 'horizontal' && Math.abs(pos.x-$scope.mouse.border) < p) {
      $scope.mouse.shift = pos.x-$scope.mouse.border;
      $scope.mouse.down = true;
    }
    else if ($scope.orient == 'vertical' && Math.abs(pos.y-$scope.mouse.border) < p) {
      $scope.mouse.shift = pos.y-$scope.mouse.border;
      $scope.mouse.down = true;
    }
  }
  function mouseup (event) {
    if (!$scope.mouse.down) return;
    $scope.mouse.down = null;
  }
  function resize (event) {
    $scope.resize();
  }

  window.addEventListener('resize', resize);
  window.addEventListener('native.keyboardshow', resize);
  window.addEventListener('native.keyboardhide', resize);

  window.addEventListener('mousedown', mousedown);
  window.addEventListener('touchstart', mousedown);

  window.addEventListener('mouseup', mouseup);
  window.addEventListener('touchend', mouseup);

  window.addEventListener('mousemove', mousemove);
  window.addEventListener('touchmove', mousemove);

  $scope.start = 0;
  $scope.end = 0;

  $scope.params = {};
  $scope.params.selected = {};
  $scope.params.stat = null;
  $scope.params.day = null;
  $scope.params.drug_id = null;
  $scope.params.drugs = {};
  $scope.params.mode = 'day';
  $scope.params.today = new Date();

  var show_day_drugs = $rootScope.storage.get(['settings', 'show_day_drugs']);
  $scope.params.show_day_drugs = show_day_drugs != null ? show_day_drugs : true;

  $scope.resize = function (f, border) {
    var cw = window.innerWidth,
        ch = window.innerHeight,
        th = document.getElementsByClassName('week-title')[0].scrollHeight,
        hh = document.getElementsByTagName('ion-header-bar')[0].scrollHeight,
        fh = document.getElementsByClassName('footer-modes')[0].scrollHeight,
        ah = ch-hh;
    if (window.innerHeight > window.innerWidth) {
      $scope.orient = 'vertical';

      var h1, h2;
      if (border) {
        if (border < (50+hh)) border = 50+hh;
        else if (border > (ah-50)) border = (ah-50);
        h1 = border-hh;
        h2 = ah - h1;
        console.log(border);
        $scope.mouse.percent = h1 / ah;
      } else {
        h1 = parseInt(ah*$scope.mouse.percent);
        h2 = ah - h1;
      }
      $scope.mouse.border = hh+h1-2;

      $scope.block1_height = h1+'px';
      $scope.block1_width = cw+'px';

      $scope.scroll1_height = (h1-th-4)+'px';
      $scope.scroll1_width = cw+'px';

      $scope.block2_height = h2 +'px';
      $scope.block2_width = cw+'px';

      $scope.scroll2_height = (h2-fh)+'px';
      $scope.scroll2_width = cw+'px';

    } else {
      $scope.orient = 'horizontal';

      var w1, w2;
      if (border) {
        if (border < 50) border = 50;
        else if (border > (cw-50)) border = (cw-50);
        w1 = border;
        w2 = cw - w1;
        $scope.mouse.percent = w1 / cw;
      } else {
        w1 = parseInt(cw*$scope.mouse.percent);
        w2 = cw - w1;
      }
      $scope.mouse.border = w1-2;

      $scope.block1_height = ah+'px';
      $scope.block1_width = w1+'px';

      $scope.scroll1_height = (ah-th)+'px';
      $scope.scroll1_width = (w1-4)+'px';

      $scope.block2_height = ah+'px';
      $scope.block2_width = w2+'px';

      $scope.scroll2_height = (ah-fh)+'px';
      $scope.scroll2_width = w2+'px';
    }
    if (!f) $scope.$apply();

    $timeout(function () {$ionicScrollDelegate.resize()}, 200);
  };
  $scope.resize(true);

  $scope.selectDay = function (day, f) {
    $scope.params.day = day;

    if ($scope.params.mode == 'day') return;

    $scope.params.drug_id = null;
    if (!f) {
      $scope.params.selected = {};
    }

    if (day.date in $scope.params.selected)
      delete $scope.params.selected[day.date];
    else
      $scope.params.selected[day.date] = day;

    $scope.calcStat();
  };
  $scope.selectWeek = function (week, f) {
    $scope.params.drug_id = null;
    $scope.params.day = null;
    var s = false;
    for (var i in week.days) {
      if (!(week.days[i].date in $scope.params.selected)) {
        s = true;
        break;
      }
    }

    if (!s) {
      if (!f) $scope.params.selected = {};
      for (var i in week.days) {
        if (week.days[i].date in $scope.params.selected)
          delete $scope.params.selected[week.days[i].date];
      }
    } else {
      if (!f) $scope.params.selected = {};
      for (var i in week.days) {
        if (!(week.days[i].date in $scope.params.selected))
          $scope.params.selected[week.days[i].date] = week.days[i];
      }
    }
    $scope.calcStat();
  };

  $scope.selectDrug = function (drug_id) {
    $scope.params.drug_id = drug_id;
    $scope.params.selected = {};
    for (var i in $scope.weeks) {
      for (var j in $scope.weeks[i].days) {
        for (var k in $scope.weeks[i].days[j].drugs) {
          if ($scope.weeks[i].days[j].drugs[k].id == drug_id) {
            $scope.params.selected[$scope.weeks[i].days[j].date] = $scope.weeks[i].days[j];
            break;
          }
        }
      }
    }
    $scope.calcStat();
  };

  $scope.resetSelected = function () {
    $scope.params.drug_id = null;
    $scope.params.selected = {};
    $scope.calcStat();
  };

  $scope.selectedDay = function (day) {
    return day.date in $scope.params.selected;
  };
  $scope.activatedDay = function (day) {
    return $scope.params.day && $scope.params.day.date == day.date;
  };
  $scope.getSelectedCount = function () {
    return Object.keys($scope.params.selected).length;
  };

  $scope.current = function (date) {
    var d = new Date(date * 1000);
    return $scope.params.today.getDate() == d.getDate() && $scope.params.today.getMonth() == d.getMonth() && $scope.params.today.getYear() == d.getYear()
  };
  $scope.getDate = function (date) {
    var d = new Date(date * 1000);
    var ms = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
    return d.getDate()+'<br><b class="month">'+(d.getDate() == 1 ? ms[d.getMonth()] : '&nbsp;')+'</b>';
  };
  $scope.oddMonth = function (date) {
    return (new Date(date * 1000)).getMonth() % 2 == 1;
  };

  $scope.calcStat = function () {
    $scope.params.stat = {};
    for (id in $scope.params.drugs) {
      $scope.params.stat[id] = $scope.params.drugs[id];
      $scope.params.stat[id].total = 0;
    }

    if (Object.keys($scope.params.selected).length > 0) {
      for (var i in $scope.params.selected) {
        for (var j in $scope.params.selected[i].drugs) {
          for (k in $scope.params.selected[i].drugs[j].doses) {
            $scope.params.stat[$scope.params.selected[i].drugs[j].id].total += $scope.params.selected[i].drugs[j].doses[k].amount;
          }
        }
      }
    }
  };

  $ionicLoading.show({template: 'Загрузка'});
  $http.get(api('v2/course/'+$stateParams.id+'/'))
    .success(function(data) {
      if (data.status == 'success') {
        $scope.access = data.access;
        $scope.course = data.course;
        $scope.weeks = data.weeks;
        $scope.end = data.end;
        $scope.start = data.start;
        $scope.drug_list = data.drugs;
        $scope.measures = data.measures;
        $scope.calcStat();
      } else if (data.status == 'error') {
        if (data.type == 'auth_error') {
          $rootScope.logout();
        }
        $rootScope.showError(data.message);
      }
    })
    .error(function () {
      $rootScope.showError('Ошибка сети');
    })
    .finally(function () {
      $ionicLoading.hide();
    });

  $scope.colors = [ '#7FFFD4', '#7FFF00', '#FF7F24', '#00FFFF', '#CAFF70', '#BF3EFF', '#FF1493',
                    '#7FFFD4', '#7FFF00', '#FF7F24', '#00FFFF', '#CAFF70', '#BF3EFF', '#FF1493'];

  $scope.getDrug = function (drug) {
    if (!(drug.id in $scope.params.drugs)) {
      $scope.params.drugs[drug.id] = drug;
      $scope.params.drugs[drug.id].color = $scope.colors.pop();
    }
    return $scope.params.drugs[drug.id];
  };

  // обновление по дням
  $scope.updateDays = function (days) {
    for (var i in $scope.weeks) {
      for (var j in $scope.weeks[i].days) {
        if ($scope.weeks[i].days[j].date in days) {
          $scope.weeks[i].days[j].drugs = days[$scope.weeks[i].days[j].date];
        }
      }
    }
    for (var date in $scope.params.selected) {
      if (date in days) {
        $scope.params.selected[date].drugs = days[date];
      }
    }
    $scope.calcStat();
  };

  $scope.loadDown = function () {
    if (!$scope.end) {
      return;
    }
    $scope.params.loadingDown = true;

    $http.get(api('v2/course/'+$stateParams.id+'/down/'+$scope.end+'/'))
      .success(function(data) {
        if (data.status == 'success') {
          $scope.weeks = $scope.weeks.concat(data.weeks);
          $scope.end = data.end;
          if ($scope.params.drug_id) {
            $scope.selectDrug($scope.params.drug_id);
          }
          $timeout(function () {$ionicScrollDelegate.resize()}, 200);
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          if (data.type == 'finish') {
            $scope.end = null;
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Ошибка сети.');
      })
      .finally(function() {
        $scope.params.loadingDown = false;
      });
  };

  $scope.loadUp = function () {
    if (!$scope.start) {
      return;
    }
    $scope.params.loadingUp = true;

    $http.get(api('v2/course/'+$stateParams.id+'/up/'+$scope.start+'/'))
      .success(function(data) {
        if (data.status == 'success') {
          $scope.weeks = data.weeks.concat($scope.weeks);
          $scope.start = data.start;
          if ($scope.params.drug_id) {
            $scope.selectDrug($scope.params.drug_id);
          }
          $timeout(function () {$ionicScrollDelegate.resize()}, 200);
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          if (data.type == 'finish') {
            $scope.start = null;
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Ошибка сети.');
      })
      .finally(function() {
        $scope.params.loadingUp = false;
      });
  };

  $scope.deleteDoses = function (drug_id) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Удаление приемов',
      template: 'Вы дейстивительно хотите удалить приемы лекарств безвозвратно?'
    });

    confirmPopup.then(function(res) {
      if(!res) return;
      $ionicLoading.show({template: 'Сохранение'});
      var data = {
        drug_id: drug_id,
        dates: Object.keys($scope.params.selected)
      };
      var config = {
        headers: {
          'X-CSRFToken': $rootScope.csrftoken
        }
      };
      $http.post(api('v2/course/'+$stateParams.id+'/doses/delete/'), data, config)
        .success(function(data) {
          if (data.status == 'success') {
            $scope.updateDays(data.days);
          } else if (data.status == 'error') {
            if (data.type == 'auth_error') {
              $rootScope.logout();
            }
            $rootScope.showError(data.message);
          }
        })
        .error(function () {
          $rootScope.showError('Ошибка сети.');
        })
        .finally(function() {
          $ionicLoading.hide();
        });
    });
  };

  // добавление приема
  $ionicModal.fromTemplateUrl('templates/spm/add_form.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.addFormModal = modal;
  });

  $scope.resetRegimen = function () {
    $scope.regimen = {
      'name': '',
      'measure': '',
      'drug_id': null,
      'new_drug': null,
      'doses': [{'amount': null, 'comment': null}]
    };
  };
  $scope.resetRegimen();
  $scope.regimenAddDose = function () {
    $scope.regimen.doses.push({'amount': null, 'comment': null});
  };
  $scope.regimenDelDose = function (ind) {
    $scope.regimen.doses.splice(ind, 1);
  };
  $scope.regimenShowAddForm = function () {
    if (Object.keys($scope.params.selected).length == 0) {
      $rootScope.showInfo('Перед тем как добавить прием, выберите дни в календаре');
      $scope.selectTab('period');
      return;
    }
    $scope.resetRegimen();
    $scope.addFormModal.show()
      .then(function () {
        document.getElementById('drug_input').focus()
      });
  };
  $scope.regimenHideAddForm = function () {
    $scope.addFormModal.hide();
  };
  $scope.regimenSearchDrug = function () {
    $scope.regimen.drugs = $filter('smartSearch')(
      $scope.drug_list, $scope.regimen.name.split(' '),
      {name: true, short_name: true}
    );
    if ($scope.regimen.drugs) {
      $scope.regimen.drugs.splice(10);
    }
  };
  $scope.regimenSelectDrug = function (drug) {
    $scope.regimen.name = drug.name;
    $scope.regimen.measure = drug.measure;
    $scope.regimen.drug_id = drug.id;
    $scope.regimen.new_drug = null;
    $scope.regimen.drugs = [];
  };
  $scope.regimenNewDrug = function () {
    $scope.regimen.new_drug = {
      'name': $scope.regimen.name,
      'short_name': '',
      'measure': 'eд'
    };
  };
  $scope.regimenSend = function () {
    var data = {
      'doses': $scope.regimen.doses,
      'dates': Object.keys($scope.params.selected)
    };
    if ($scope.regimen.drug_id) {
      data.drug_id = $scope.regimen.drug_id;
      data.measure = $scope.regimen.measure;
    } else if ($scope.regimen.new_drug) {
      data.new_drug = $scope.regimen.new_drug;
    } else {
      $scope.showError('Не выбран медикамент');
      return;
    }

    var config = {
      headers: {
        'X-CSRFToken': $rootScope.csrftoken
      }
    };
    $ionicLoading.show({template: 'Сохранение'});
    $http.post(api('v2/course/'+$stateParams.id+'/doses/add/'), data, config)
      .success(function(data) {
        if (data.status == 'success') {
          if (data.drug) {

            if (data.drug.id in $scope.params.drugs) {
              // меняем меру измерения
              $scope.params.drugs[data.drug.id].measure = data.drug.measure;
            } else {
              // присваиваете цвет новому медикаменту
              $scope.getDrug(data.drug);
            }

            // добавление медикамента
            for (var i in $scope.drug_list) {
              if ($scope.drug_list[i].id == data.drug.id) {
                $scope.drug_list[i] = data.drug;
                data.drug = null;
                break;
              }
            }
            if (data.drug) {
              $scope.drug_list.push(data.drug);
            }
          }
          $scope.regimenHideAddForm();
          $scope.updateDays(data.days);
          $rootScope.showSuccess('Медикаменты добавлены');
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Ошибка сети.');
      })
      .finally(function() {
        $ionicLoading.hide();
      });
  };

  // информация о СПМ
  $ionicModal.fromTemplateUrl('templates/spm/info.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.infoModal = modal;
  });
  $scope.hideInfo = function () {
    $scope.infoModal.hide();
  };
  $scope.showInfo = function () {
    $scope.infoModal.show();
  };

  // работа с закладками
  $scope.selectTab = function (mode) {
    $scope.params.mode = mode;
    $timeout(function () {$ionicScrollDelegate.resize()}, 200);
  };

  // отметка о приеме лекарства
  $scope.doseMark = function (dose) {
    if (!$scope.access.mark) return;
    dose.loading = true;
    $http.get(api('v2/dose/mark/?id='+dose.id))
      .success(function(data) {
        if (data.status == 'success') {
          dose.is_marked = data.is_marked;
          dose.marked_at = data.marked_at;
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Ошибка сети');
      })
      .finally(function () {
        dose.loading = false;
      });
  };

  // редактирование данных
  $scope.changeAuthor = function () {
    userSelect.init(
      'Выберите редактора',
      function (user) {
        $scope.course.author = user;
      }
    ).show();
  };

  $scope.changeSportsman = function () {
    userSelect.init(
      'Выберите спортсмена',
      function (user) {
        $scope.course.patient = user;
      }
    ).show();
  };

  $scope.saveCourse = function () {
    var data = {
      'name': $scope.course.name,
      'is_public': $scope.course.is_public,
      'author': $scope.course.author.username,
      'patient': $scope.course.patient.username
    };
    var config = {
      headers: {
        'X-CSRFToken': $rootScope.csrftoken
      }
    };
    $ionicLoading.show({template: 'Сохранение'});
    $http.post(api('v2/course/'+$stateParams.id+'/edit/'), data, config)
      .success(function(data) {
        if (data.status == 'success') {
          $scope.course = data.course;
          $scope.access = data.access;
          $rootScope.showSuccess('Изменения сохранены');
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Ошибка сети.');
      })
      .finally(function() {
        $ionicLoading.hide();
      });
  };

  $scope.deleteCourse = function () {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Удаление СПМ',
      template: 'Вы уверены что хотите удалить этот СПМ безвозвратно?'
    });

    confirmPopup.then(function(res) {
      if (!res) return;
      var config = {
        headers: {
          'X-CSRFToken': $rootScope.csrftoken
        }
      };
      $ionicLoading.show({template: 'Удаление'});
      $http.post(api('v2/course/'+$stateParams.id+'/delete/'), {}, config)
        .success(function(data) {
          if (data.status == 'success') {
            $scope.hideInfo();
            $ionicHistory.goBack();
          } else if (data.status == 'error') {
            if (data.type == 'auth_error') {
              $rootScope.logout();
            }
            $rootScope.showError(data.message);
          }
        })
        .error(function () {
          $rootScope.showError('Ошибка сети.');
        })
        .finally(function() {
          $ionicLoading.hide();
        });
    });
  };
});
