function secondsFormat (ss) {
  ss = parseInt(ss);
  var hours = parseInt(ss / 3600) % 24;
  var minutes = parseInt(ss/ 60) % 60;
  var seconds = parseInt(ss % 60);

  return (hours > 0 ? (hours < 10 ? "0" + hours : hours) + ":" : '')
    + ((hours > 0 && minutes < 10 ? "0" + minutes : minutes) + ":")
    + (seconds  < 10 ? "0" + seconds : seconds);
}
ctrls.directive('passedSeconds', ['$interval',
function($interval) {
  return function(scope, element, attrs) {
    var date,  // date format
        stopTime; // so that we can cancel the time updates

    function updateTime() {
      var ss = 0;
      if (date) {
        ss = ((new Date()).getTime() - date.getTime()) / 1000;
      }
      element.text(secondsFormat(ss > 0 ? ss : -ss));
    }

    scope.$watch(attrs.passedSeconds, function(value) {
      date = value;
      updateTime();
    });

    stopTime = $interval(updateTime, 1000);

    element.on('$destroy', function() {
      $interval.cancel(stopTime);
    });
  }
}]);
ctrls.controller('TrainShowCtrl', function($scope, $rootScope, $http, $ionicPopup, $ionicModal, $stateParams, $ionicLoading, addVideoForm, $state) {

  // сбросить итоги
  $scope.resetState = function () {
    $scope.state = {
      start: null,
      stop: null,
      timer: null,
      executions: {},
      schemes: {},
      is_saved: false
    };
  };

  $scope.settings = {
    show_warmup: true,
    show_breaks: false,
    try_done: false,
    try_timer: false
  };
  $scope.resetState();

  $scope.getNumber = function(num) {
    return new Array(num);
  };
  $scope.getWarmupNumber =function (e) {
    var s = [];
    for (var i = 0; i < e.warmup_scheme.length; i++) {
      for (var j = 0; j < e.warmup_scheme[i].apps; j++) {
        s.push({
          weight: e.warmup_scheme[i].weight,
          reps: e.warmup_scheme[i].reps
        });
      }
    }
    return s;
  };

  $scope.startTrain = function () {
    // фиксируем что тренировка запущена
    $rootScope.storage.set(['current', 'train', 'id'], $stateParams.id);

    $rootScope.showInfo($scope.messages.scheme_press);

    $scope.state.start = new Date();
    $scope.state.timer = new Date();
    $scope.state.stop = null;
    $scope.state.is_saved = false;

    $scope.saveTrainState();
  };
  $scope.stopTrain = function () {
    // удаляем запущенную тренировку
    $rootScope.storage.remove(['current', 'train', 'id']);

    $scope.state.stop = new Date();
    $scope.state.timer = null;

    $scope.showStats();
    $scope.saveTrainState();
  };

  $scope.tickTimer = function () {
    var date = new Date();
    var t = (date.getTime() - $scope.state.timer.getTime()) / 1000;
    $scope.state.timer = date;
    return t;
  };
  $scope.saveTrainState = function () {
    $rootScope.storage.set(['train_state', $stateParams.id], $scope.state);
  };

  $scope.secondsFormat = secondsFormat;
  $scope.passedSeconds = function (date) {
    return $scope.secondsFormat(( (new Date()).getTime() - date.getTime())/1000);
  };

  // нажатие на разминку
  $scope.warmupSchemeDone = function (e) {
    if (!$scope.state.timer) {
      $rootScope.showInfo($scope.messages.start_timer);
      return;
    }
    if ($scope.state.executions[e.id] == undefined) {
      $scope.state.executions[e.id] = [];
    }
    if (e.numbers.length > $scope.state.executions[e.id].length) {
      $scope.state.executions[e.id].push($scope.tickTimer());
      $scope.saveTrainState();
      $scope.settings.try_done = true;
    }
  };
  $scope.warmupSchemeCancel = function (e) {
    if (!$scope.state.timer) {
      $rootScope.showInfo($scope.messages.start_timer);
      return;
    }
    if ($scope.state.executions[e.id] != undefined) {
      $scope.state.executions[e.id].pop();
      $scope.saveTrainState();
    }
  };

  // нажатие на подходы
  $scope.schemePress = function () {
    if ($scope.state.timer) {
      if (!$scope.settings.try_done) {
        $rootScope.showInfo($scope.messages.scheme_press);
      }
    } else {
      $rootScope.showInfo($scope.messages.start_timer);
    }
  };
  $scope.schemeDone = function (scheme) {
    if (!$scope.state.timer) {
      $rootScope.showInfo($scope.messages.start_timer);
      return;
    }
    if ($scope.state.schemes[scheme.id] == undefined) {
      $scope.state.schemes[scheme.id] = [];
    }
    if (scheme.apps > $scope.state.schemes[scheme.id].length) {
      $scope.state.schemes[scheme.id].push($scope.tickTimer());
      $scope.saveTrainState();
      $scope.settings.try_done = true;
    }
  };
  $scope.schemeCancel = function (scheme) {
    if (!$scope.state.timer) {
      $rootScope.showInfo($scope.messages.start_timer);
      return;
    }
    if ($scope.state.schemes[scheme.id] != undefined) {
      $scope.state.schemes[scheme.id].pop();
      $scope.saveTrainState();
    }
  };

  // статистика
  $ionicModal.fromTemplateUrl('templates/train/stats.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.statsModal = modal;
  });

  $scope.showStats = function () {
    $scope.st = {};

    // можно ли сохранять
    $scope.can_record_state = false;
    if (!$scope.state.is_saved && $scope.state.start && $scope.state.stop) {
      $scope.can_record_state = true;
    }

    // примерное время
    if ($scope.state.start) {
      $scope.st.apr_seconds = ( (new Date()).getTime() - $scope.state.start.getTime() ) / 1000;
    } else {
      $scope.st.apr_seconds = 0;
    }
    $scope.state.kps = 0;
    $scope.state.tonag = 0;
    angular.forEach($scope.train.executions, function (e) {
      angular.forEach(e.scheme, function (s) {
        var apps = s.apps;
        if (s.id in $scope.state.schemes) {
          var scheme = $scope.state.schemes[s.id];

          $scope.state.kps += s.reps * scheme.length;
          $scope.state.tonag += s.weight * s.reps * scheme.length * (e.exercise.is_dual ? 2 : 1);

          apps -= scheme.length;
        }

        if (apps > 0) {
          if (s.percent < 45)       p = 60;
          else if (s.percent < 55)  p = 100;
          else if (s.percent < 65)  p = 120;
          else if (s.percent < 75)  p = 140;
          else if (s.percent < 85)  p = 160;
          else                      p = 180;
          $scope.st.apr_seconds += apps * p;
        }

      });
    });

    $scope.st.tonag_percent = parseInt($scope.state.tonag / $scope.train.tonag * 100);
    $scope.st.kps_percent = parseInt($scope.state.kps / $scope.train.kps * 100);

    if ($scope.state.start && $scope.state.stop) {
      $scope.st.duration = ($scope.state.stop.getTime() - $scope.state.start.getTime())/1000;
    } else {
      $scope.st.duration = null;
    }

    if ($scope.state.start && $scope.state.timer) {
      $scope.st.apr_stop = new Date();
      $scope.st.apr_stop.setTime($scope.state.start.getTime() + $scope.st.apr_seconds*1000);
      var left_seconds = ((new Date()).getTime() - $scope.state.start.getTime()) / 1000;
      $scope.st.time_percent = parseInt(left_seconds / $scope.st.apr_seconds * 100);
    } else {
      $scope.st.apr_stop = null;
      $scope.st.time_percent = null;
    }
    $scope.statsModal.show();
  };
  $scope.closeStats = function () {
    $scope.statsModal.hide();
  };

  // итоги тренировки
  $scope.recordState = function () {
    var data = {
      start: $scope.state.start.getTime(),
      end: $scope.state.stop.getTime(),
      resume: $scope.train.resume,
      time_breaks: $scope.state.schemes
    };
    $scope.saveTrainState();

    $ionicLoading.show({template: 'Сохранение...'});

    $http.post(api('train/' + $stateParams.id + '/record/'), data)
      .success(function(data) {
        if (data.status == 'success') {
          $rootScope.showInfo('Данные сохранены');
          $scope.closeStats();

          $scope.train = data.train;
          $rootScope.storage.set(['train', $scope.train.id], data.train);

          $scope.state.is_saved = true;
          $scope.saveTrainState();
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Ошибка при сохранении. Попробуйте позже');
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  // показывать время отдыха
  $scope.showBreaks = function () {
    $scope.settings.show_breaks = true;
    if (!$scope.state.try_timer) {
      $rootScope.showInfo($scope.messages.show_breaks);
    }
  };
  $scope.hideBreaks = function () {
    $scope.settings.show_breaks = false;
  };

  // ---- инициализация
  $scope.today = new Date(); // для вывода начала и конца тренировки
  $scope.messages = {
    start_timer: 'Чтобы отмечать подходы начните тренировку',
    stop_timer: 'Удерживайте чтобы закончить',
    scheme_press: 'Удерживайте-чтобы выполнить<br>Смахните влево-чтобы отменить',
    show_breaks: 'Удерживайте <i class="ion-ios-stopwatch-outline"></i>, чтобы увидеть время отдыха между подходами',
  };

  $scope.update = function (refresh) {

    if (!refresh) {
      var train = $rootScope.storage.get(['train', $stateParams.id], true);
      if (train) {
        $ionicLoading.hide();
        $scope.train = train;

        // устанавливаем состояние если есть
        var state = $rootScope.storage.get(['train_state', $stateParams.id]);
        if (state) {
          $scope.state = {
            start: state.start ? new Date(parseInt(Date.parse(state.start))) : state.start,
            stop: state.stop ? new Date(parseInt(Date.parse(state.stop))) : state.stop,
            timer: state.timer ? new Date(parseInt(Date.parse(state.timer))) : state.timer,
            executions: state.executions,
            schemes: state.schemes,
            is_saved: state.is_saved
          };
          $rootScope.showInfo('Тренировка восстановлена');
        } else {
          $scope.setTrainState(train);
          $scope.saveTrainState();
        }
        return;
      }
    }

    $http({method: 'get', url: api('v2/train/' + $stateParams.id+'/'), timeout: 10000})
      .success(function (data) {
        if (data.status == 'success') {
          $scope.train = data.train;
          $rootScope.storage.set(['train', $stateParams.id], $scope.train);

          // работа с состоянием
          // текущее состояние пустое
          if ($scope.state.start == null && $scope.state.stop == null) {
            $scope.setTrainState(data.train);
            $scope.saveTrainState();
            return;
          }

          // текущее состояние сохранено
          if ($scope.state.start != null && $scope.state.stop != null && $scope.state.is_saved) {
            $scope.setTrainState(data.train);
            $scope.saveTrainState();
            return;
          }

          // тренировка активна в данный момент
          if ($scope.state.start != null && $scope.state.stop == null && $scope.state.timer) {
            var confirmPopup = $ionicPopup.confirm({
              title: 'Тренировка выполняется',
              template: 'Вы хотите сбросить текущее выполнение тренировки?'
            });

            confirmPopup.then(function(res) {
              if(res) {
                $scope.setTrainState(data.train);
                $scope.saveTrainState();
              }
            });
            return;
          }

          // тренировка завершена но не сохранена
          if ($scope.state.start != null && $scope.state.stop != null && $scope.state.timer == null && !$scope.state.is_saved) {
            var confirmPopup = $ionicPopup.confirm({
              title: 'Тренировка выполнена, но не сохранена',
              template: 'Обновление данных сотрет текущее состояние тренировки. Вы хотите сохранить состояние тренировки в Workspace?'
            });

            confirmPopup.then(function(res) {
              if(res) {
                $scope.recordState();
              } else {
                $scope.setTrainState(data.train);
                $scope.saveTrainState();
              }
            });
            return;
          }
          console.log('vstop');

        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        var train = $rootScope.storage.get(['train', $stateParams.id], true);
        $scope.train = train;
        if (train) {
          $rootScope.showInfo('Ошибка сети: тренировка загружена из кеша');
        } else {
          $rootScope.showError('Не удалось загрузить тренировку.<br>Попробуйте снова');
        }
      })
      .finally(function () {
        $scope.$broadcast('scroll.refreshComplete');
      });
  };

  // установка состояния тренировки
  $scope.setTrainState = function (train) {
    $scope.state.start = train.start ? new Date(train.start * 1000) : null;
    $scope.state.stop = train.end ? new Date(train.end * 1000) : null;
    $scope.state.schemes = train.time_breaks;
    $scope.state.timer = null;
    $scope.state.is_saved = true;
  };

  $scope.update();

  var show_warmup = $rootScope.storage.get(['settings', 'show_warmup']);
  if (show_warmup != null) {
    $scope.settings.show_warmup = show_warmup;
  }

  // сброс тренировки
  $scope.clearState = function () {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Сброс итогов тренировки',
      template: 'Вы уверены что хотите сбросить?'
    });

    confirmPopup.then(function(res) {
      if(res) {
        $scope.resetState();
        $scope.train.is_done = false;
        $scope.closeStats();
      }
    });
  };

  // загрузка видео
  $scope.showSchemeMenu = function (execution, scheme, app) {
    $scope.schemeMenu = {};
    $scope.schemeMenu.execution = execution;
    $scope.schemeMenu.scheme = scheme;
    $scope.schemeMenu.app = app;
    $scope.schemeMenu.video_id = scheme.videos[app] ? scheme.videos[app].id : false;
    $scope.schemeMenu.add_video = true;

    $scope.schemeMenu.popup = $ionicPopup.show({
      scope: $scope,
      title: scheme.weight+'кг на '+scheme.reps+' ('+app+' подход)',
      templateUrl: 'templates/train/scheme_menu.html'
    });
  };
  $scope.hideSchemeMenu = function () {
    $scope.schemeMenu.popup.close();
  };

  $scope.addVideo = function (execution, scheme, app) {
    addVideoForm.showForm({
      exercise_id: execution.exercise.id,
      exercise_name: execution.exercise.name,
      reps: scheme.reps,
      weight: scheme.weight,
      scheme_id: scheme.id,
      app: app,
      test: 'test',
      onsuccess: function (video_id) {
        scheme.videos[app] = {id: video_id};
        $scope.$apply();
        $rootScope.storage.set(['train', $stateParams.id], $scope.train);
      }
    });
  };
});
