ctrls.controller('UserListCtrl', function($rootScope, $scope, $http) {
  $scope.data = {
    search_loading: false,
    query: ''
  };
  
  $scope.search = function () {
    $scope.data.search_loading = true;
    $http.get(api('v2/user/search/?q='+$scope.data.query))
      .success(function (data) {
        $scope.user_list = data.list;
      })
      .error(function () {
        $rootScope.showError('Ошибка сети при поиске пользователя.');
      })
      .finally(function () {
        $scope.data.search_loading = false;
      });
  };
  $scope.search();
});
