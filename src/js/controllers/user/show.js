ctrls.controller('UserShowCtrl', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $sce) {
  $ionicLoading.show({template: 'Загрузка...'});
  $http.get(api('v2/user/'+$stateParams.id+'/'))
    .success(function(data) {
      if (data.status == 'success') {
        $scope.cuser = data.user;
        $scope.badges = data.badges;
      } else if (data.status == 'error') {
        if (data.type == 'auth_error') {
          $rootScope.logout();
        }
        $rootScope.showError(data.message);
      }
    })
    .finally(function () {
      $ionicLoading.hide();
    });

  $scope.selectPlan = function () {
    $ionicLoading.show({template: 'Загрузка...'});
    $http.get(api('v2/user/'+$stateParams.id+'/plans/'))
      .success(function(data) {
        if (data.status == 'success') {
          $scope.plans = data.list;
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  $scope.selectCourse = function () {
    $ionicLoading.show({template: 'Загрузка...'});
    $http.get(api('v2/user/'+$stateParams.id+'/courses/'))
      .success(function(data) {
        if (data.status == 'success') {
          $scope.courses = data.list;
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  $scope.selectVideo = function () {
    $ionicLoading.show({template: 'Загрузка...'});
    $http.get(api('v2/user/'+$stateParams.id+'/videos/'))
      .success(function(data) {
        if (data.status == 'success') {
          $scope.videos = data.list;
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  $scope.selectStudent = function () {
    $ionicLoading.show({template: 'Загрузка...'});
    $http.get(api('v2/user/'+$stateParams.id+'/students/'))
      .success(function(data) {
        if (data.status == 'success') {
          $scope.students = data.list;
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };

  $scope.getImageSrc = function (youtube_id) {
    return $sce.trustAsResourceUrl('https://i.ytimg.com/vi/' + youtube_id + '/hqdefault.jpg');
  };

  $scope.selectWeightStat = function () {
    function plotInit() {
      data = [];
      for (i = 0; i < $scope.stat.length; i++) {
        var d = new Date($scope.stat[i].date*1000);
        var utc = Date.UTC(
          d.getUTCFullYear(),
          d.getUTCMonth(),
          d.getUTCDate()
        );
        data.push([utc, $scope.stat[i].weight]);
      }
      Highcharts.chart('plot_' + $scope.cuser.id, {
          title: {
              text: 'Вес (кг)'
          }, chart: {
              type: 'spline'
          },
          rangeSelector: {
              selected: 1
          },
          xAxis: {
              type: 'datetime',
              title: false
          },
          yAxis: {
              title: false,
              endOnTick: false,
              startOnTick: false
          },
          series: [{
              showInLegend: false,
              data: data
          }]
      });
    };

    $ionicLoading.show({template: 'Загрузка...'});
    $http.get(api('v2/user/'+$stateParams.id+'/weight-stat/'))
      .success(function(data) {
        if (data.status == 'success') {
          $scope.stat = data.list;
          plotInit();
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  };
});
