ctrls.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});

ctrls.controller('UtilConverterCtrl', function($scope) {
  $scope.list = [
    {'name': 'Тестостерон общий', 'master_unit': 'нмоль/л', 'slave_units': ['нг/мл', 'мкг/л'], 'rate': 3.467},
    {'name': 'Эстрадиол', 'master_unit': 'пмоль/л', 'slave_units': ['пг/мл', 'нг/л'], 'rate': 3.671},
    {'name': 'Пролактин', 'master_unit': 'мЕд/л', 'slave_units': ['нг/мл', 'мкг/л'], 'rate': 21.2},
    {'name': 'ЛГ ', 'master_unit': 'мЕд/мл', 'slave_units': ['Ед/л'], 'rate': 1.0},
    {'name': 'Билирубин', 'master_unit': 'мкмоль/л', 'slave_units': ['мг/дл'], 'rate': 17.094017},
    {'name': 'Соматотропин', 'master_unit': 'мМЕ/л', 'slave_units': ['нг/мл', 'мкг/л'], 'rate': 2.6}
  ];
  $scope.changeMaster = function (obj) {
    obj.slave_value = (obj.master_value / obj.rate).toFixed(2);
  };

  $scope.changeSlave = function (obj) {
    obj.master_value = (obj.slave_value * obj.rate).toFixed(2);
  };

});
