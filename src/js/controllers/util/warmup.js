ctrls.controller('UtilWarmupCtrl', function($scope, $rootScope, ionicTimePicker) {
  $scope.calcWarmup = function () {
    if ($scope.data.weight > 500) {
      $scope.data.weight = 500;
    }
    if ($scope.data.reps > 100) {
      $scope.data.reps = 100;
    }
    $rootScope.storage.set(['warmup', 'weight'], $scope.data.weight);
    $rootScope.storage.set(['warmup', 'reps'], $scope.data.reps);
    $rootScope.storage.set(['warmup', 'start_time'], $scope.data.start_time.getTime());
    $rootScope.storage.remove(['warmup', 'current_scheme_num']);

    $scope.data.warmup = $scope.warmup(parseInt($scope.data.weight), parseInt($scope.data.reps), $scope.data.start_time);
    $scope.data.current_scheme_num = 0;
    $rootScope.showSuccess('Разминка расчитана');
  };

  $scope.warmup = function (weight, reps, start_time) {
    // веса
    var ws = [];
    if (weight >= 50) {
      var steps = [20, 30];
      var w = 0;
      var ind = 0;
      for (;;) {
        w += steps[ind % steps.length];
        if (w >= weight) {
          break;
        }
        ws.push(w);
        ind += 1;
      }

      var last_step = weight - ws[ws.length-1];
      if (last_step < 20 && ws.length > 1) {
        var s = weight - ws[ws.length-2];
        ws[ws.length-1] = round_to(ws[ws.length-2] + s / 2, 5);
      } else {
        var s = weight - ws[ws.length-1];
        ws.push(round_to(ws[ws.length-1] + s / 2, 5));
      }
    } else {
      ws.push(round_to(weight * 0.75, 2));
    }

    // повторения
    var res = [];
    var r, i, ind;
    if (reps <= 7) {
      for (i = 0; i < ws.length; i++) {
        r = Math.min(ws.length - i + reps - 1, 6);
        res.push({weight: ws[i], reps: r});
        if (ws[i] == 20 || ws[i] == 50) res.push({weight: ws[i], reps: r});
      }
    } else {
      for (i = 0; i < ws.length; i++) {
        ind = ws.length - i - 1;
        if (reps >= 12) {
          r = 12;
        }
        else if (reps >= 10) {
          r = ind == 0 ? 10 : 12;
        }
        else if (reps >= 8) {
          if (ind == 0) {
            r = 8;
          }
          else if (ind == 1) {
            r = 10;
          }
          else {
            r = 12;
          }
        }
        res.push({weight: ws[i], reps: r});
        if (ws[i] == 20 || ws[i] == 50) res.push({weight: ws[i], reps: r});
      }
    }

    // время
    for (i = res.length-1; i >= 0; i--) {
      var m = 0;
      if (res[i]['weight'] < 70)        m = 3;
      else if (res[i]['weight'] < 120)  m = 3;
      else if (res[i]['weight'] < 170)  m = 4;
      else if (res[i]['weight'] < 220)  m = 5;
      else                              m = 6;
      var t = new Date(start_time.getTime() - m*60000);
      res[i]['time'] = t;
      start_time = t;
    }

    return res;
  };

  $scope.timeFormat = function (t) {
    var h = t.getHours(),
        m = t.getMinutes();
    return h + ':' + (m < 10 ? m = '0' + m : m);
  };

  $scope.convertTime = function (date) {
      var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

      var offset = date.getTimezoneOffset() / 60;
      var hours = date.getHours();

      newDate.setHours(hours + offset);

      return newDate;
  };

  $scope.showStartTime = function () {
    var opts = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          $rootScope.showError('Время не установлено.');
        } else {
          $scope.data.start_time = $scope.convertTime(new Date(val * 1000));
        }
      },
      inputTime: ($scope.data.start_time.getHours() * 60 * 60) + (round_to($scope.data.start_time.getMinutes(), 5) * 60),
      format: 24,
      step: 5,
      setLabel: 'Установить',
      closeLabel: 'Закрыть'
    };
    ionicTimePicker.openTimePicker(opts);
  };

  $scope.clickScheme = function (ind) {
    $scope.data.current_scheme_num = ind+1;
    $rootScope.storage.set(['warmup', 'current_scheme_num'], $scope.data.current_scheme_num);
    if ($scope.data.current_scheme_num == $scope.data.warmup.length) {
      $rootScope.showSuccess('Бразер, удачи на помосте!!!');
    }
  };

  // init
  $scope.data = {
    start_time: $rootScope.storage.get(['warmup', 'start_time'])
      ? new Date($rootScope.storage.get(['warmup', 'start_time']))
      : new Date(),
    weight: $rootScope.storage.get(['warmup', 'weight']) || 0,
    reps: $rootScope.storage.get(['warmup', 'reps']) || 0,
    current_scheme_num: $rootScope.storage.get(['warmup', 'current_scheme_num']) || 0,
    warmup: []
  };
  $scope.data.warmup = $scope.warmup(parseInt($scope.data.weight), parseInt($scope.data.reps), $scope.data.start_time);
});
