ctrls.controller('VideoListCtrl', function($rootScope, $scope, $http, $sce, $filter, $ionicLoading, addVideoForm, $ionicHistory) {
  $scope.getImageSrc = function (youtube_id) {
    return $sce.trustAsResourceUrl('https://i.ytimg.com/vi/' + youtube_id + '/hqdefault.jpg');
  };

  $rootScope.$on('delete_video', function (event, video_id) {
    angular.forEach($scope.all_videos, function (video, ind) {
      if (video.id == video_id) {
        $scope.all_videos.splice(ind, 1);
      }
    });
    angular.forEach($scope.videos, function (video, ind) {
      if (video.id == video_id) {
        $scope.videos.splice(ind, 1);
      }
    });
    $ionicHistory.clearCache();
  });

  $scope.refresh = function () {
    $ionicLoading.show({template: 'Загрузка...'});
    $http.get(api('video/list/'))
      .success(function (data) {
        if (data.status == 'success') {
          $scope.all_videos = data.videos;
          $scope.videos = data.videos;
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .finally(function () {
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
      });
  };
  $scope.refresh();

  $scope.query = '';
  $scope.search = function () {
    if ($scope.query) {
      $scope.videos = [];
      angular.forEach(
        $filter('smartSearch')($scope.all_videos, $scope.query.split(' '), {name: true}),
        function (item) {
          $scope.videos.push(item.entry);
        }
      );
    } else {
      $scope.videos = $scope.all_videos;
    }
  };

  $scope.showForm = function () {
    try {
      addVideoForm.showForm({
        onsuccess: function onSuccess(video_id) {
          $scope.refresh();
        }
      });
    } catch(e) {
      $scope.msg = ('Ошибка ' + e.name + ":" + e.message + "\n" + e.stack);
    }
  };
});
