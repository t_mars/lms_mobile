ctrls.controller('VideoShowCtrl', function($scope, $rootScope, $http, $stateParams, $ionicLoading, $sce, $ionicHistory, $state) {
  $ionicLoading.show({template: 'Загрузка...'});
  $scope.getIframeSrc = function (youtube_id) {
    return $sce.trustAsResourceUrl('https://www.youtube.com/embed/'+youtube_id+'?autoplay=1&fs=1&modestbranding=1&rel=0');
  };

  $http.get(api('video/show/?id='+$stateParams.id))
    .success(function(data) {
      $ionicLoading.hide();
      if (data.status == 'error') {
        $rootScope.showError(data.message);
      } else {
        $scope.video = data.video;
      }
    })
    .error(function(data) {
      $ionicLoading.hide();
    });

  $scope.deleteVideo = function () {
    function del (flag) {
      if (flag != 1) return;
      $http.get(api('video/delete/?id='+$stateParams.id))
        .success(function(data) {
          $ionicLoading.hide();
          if (data.status == 'error') {
            if (data.type == 'auth_error') {
              $rootScope.logout();
            }
            $rootScope.showError(data.message);
          } else {
            // удаление видео
            if ($ionicHistory.backView()) {
              $ionicHistory.goBack();
            } else {
              $state.go('app.video_list');
            }
            $rootScope.$emit('delete_video', $stateParams.id);
            $rootScope.showInfo('Видео удалено.');
          }
        })
        .finally(function(data) {
          $ionicLoading.hide();
        });
    }
    if (ionic.Platform.is('BROWSER')) {
      del(1);
    } else {
      navigator.notification.confirm(
        'Вы уверены что хотите удалить видео?',
         del,
        $scope.video.title,
        ['Удалить','Отмена']
      );
    }
  };

  $scope.share = function () {
    var options = {
      message: $scope.video.title,
      subject: $scope.video.title,
      url: 'http://ws.last-man.org/video/' + $scope.video.id
    };

    var onSuccess = function(result) {
      console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
      console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
    };

    var onError = function(msg) {
      console.log("Sharing failed with message: " + msg);
    };

    window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
  }
});
