ctrls.controller('WeightStatCtrl', function($scope, $rootScope, $http, $filter, $ionicPopup, $ionicLoading) {
  $scope.record = {
    date: new Date(),
    weight: ''
  };

  $scope.plot_init = function () {
    data = [];
    for (i = 0; i < $scope.stat.length; i++) {
      var d = new Date($scope.stat[i].date*1000);
      var utc = Date.UTC(
        d.getUTCFullYear(),
        d.getUTCMonth(),
        d.getUTCDate()
      );
      data.push([utc, $scope.stat[i].weight]);
    }
    Highcharts.chart('plot', {
        title: {
            text: 'Вес (кг)'
        }, chart: {
            type: 'spline'
        },
        rangeSelector: {
            selected: 1
        },
        xAxis: {
            type: 'datetime',
            title: false
        },
        yAxis: {
            title: false,
            endOnTick: false,
            startOnTick: false
        },
        series: [{
            showInLegend: false,
            data: data
        }]
    });
  };

  $scope.update = $rootScope.updateData({
    keys: ['profile', 'weight'],
    resetData: function () {$scope.stat = []},
    getData: function (data) {return data.stat},
    setData: function (data) {
      $scope.stat = data;
      $scope.plot_init();
      if (data.length) {
        $scope.record.weight = data[data.length-1].weight
      }
    },
    request: function () {return $http.get(api('profile/weight-stat/'))}
  });
  $scope.update();

  $scope.send = function () {
    $ionicLoading.show({template: 'Загрузка...'});
    data = {
      'date': $filter('date')($scope.record.date, 'dd.MM.yy'),
      'weight': $scope.record.weight
    };
    $http.post(api('profile/add-weight/'), data)
      .success(function(data) {
        if (data.status == 'success') {
          $scope.update(true);
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function (data) {
        $rootScope.showError('Ошибка отправки запроса.');
      })
      .finally(function () {
        $ionicLoading.hide();
      });
  }
});
