ctrls.controller('WsIndexCtrl', function($scope, $rootScope, $filter, $http, $ionicTabsDelegate) {
  $scope.selectTab = function (mode) {
    if (mode == 'trains') {
      $scope.toDay();
      $ionicTabsDelegate.select(0);
    } else if (mode == 'meds') {
      $scope.toDay();
      $ionicTabsDelegate.select(1);
    }
  };
  $rootScope.$on('ws_mode', function (event, mode) {
    $scope.selectTab(mode);
  });
  $scope.$on('$ionicView.enter', function() {
    if ($rootScope.ws_mode) {
      $scope.selectTab($rootScope.ws_mode);
      $rootScope.ws_mode = '';
    } else {
      $scope.update('hidden_refresh');
    }
  });

  $scope.getDateStr = function () {
    var d = new Date();
    var dp = new Date();dp.setDate(d.getDate() - 1);
    var dn = new Date();dn.setDate(d.getDate() + 1);

    if ($scope.date.setHours(0,0,0,0) == d.setHours(0,0,0,0)) {
      return 'Сегодня, ' + $filter('date')($scope.date, 'dd.MM');
    }
    else if ($scope.date.setHours(0,0,0,0) == dp.setHours(0,0,0,0)) {
      return 'Вчера, ' + $filter('date')($scope.date, 'dd.MM');
    }
    else if ($scope.date.setHours(0,0,0,0) == dn.setHours(0,0,0,0)) {
      return 'Завтра, ' + $filter('date')($scope.date, 'dd.MM');
    }

    return $filter('date')($scope.date, 'EEE, dd.MM');
  };

  $scope.toDay = function () {
    $scope.date = new Date();
    $scope.update('hidden_refresh');
  };

  $scope.prevDay = function () {
    $scope.date.setDate($scope.date.getDate() -1);
    $scope.update('hidden_refresh');
  };

  $scope.nextDay = function () {
    $scope.date.setDate($scope.date.getDate() +1);
    $scope.update('hidden_refresh');
  };

  $scope.update = $rootScope.updateData({
    getKeys: function () {
      return ['ws', $filter('date')($scope.date, 'dd.MM.yy')]
    },
    resetData: function () {
      $scope.trains = [];
      $scope.meds = [];
    },
    getData: function (data) {
      angular.forEach(data.trains, function(train) {
        $rootScope.storage.set(['train', train.id], train);
      });

      return data;
    },
    setData: function (data) {
      angular.forEach(data.trains, function(train, ind) {
        var t = $rootScope.storage.get(['train', train.id]);
        if (t) {
          data.trains[ind] = t;
        }
      });

      $scope.meds = data.meds;
      $scope.trains = data.trains;
      $scope.resetBadges();
    },
    request: function () {
      return $http.get(api('v2/ws/?date='+$filter('date')($scope.date, 'dd.MM.yy')))
    }
  });

  //
  $scope.resetBadges = function () {
    var data = {
      'trains_count': $scope.trains.length,
      'trains_done': 0,
      'meds_count': 0,
      'meds_done': 0
    };

    if (data.trains_count) {
      angular.forEach($scope.trains, function (train) {
        if (train.is_done) {
          data.trains_done += 1;
        }
      });
    }
    if (data.trains_count > 0 && data.trains_done > 0) {
      $scope.trains_badge = data.trains_done + '/' + data.trains_count;
    } else {
      $scope.trains_badge = data.trains_count;
    }

    if ($scope.meds.length) {
      angular.forEach($scope.meds, function (course) {
        angular.forEach(course.drugs, function (drug) {
          angular.forEach(drug.doses, function (dose) {
            if (dose.is_marked != null) {
              data.meds_done += 1;
            }
            data.meds_count += 1
          });
        });
      });
    }

    if (data.meds_count > 0 && data.meds_done > 0) {
      $scope.meds_badge = data.meds_done + '/' + data.meds_count;
    } else {
      $scope.meds_badge = data.meds_count;
    }

    // перебиваем бейджи в меню
    if ($scope.date.setHours(0,0,0,0) == $rootScope.date.setHours(0,0,0,0)) {
      $rootScope.setCounts(data);
    }
  };

  // сохраняем в кеш тренировки и медикаменты по дате
  $scope.saveCache = function () {
    $rootScope.storage.set(
      ['ws', $filter('date')($scope.date, 'dd.MM.yy')],
      {'meds': $scope.meds, 'trains': $scope.trains}
    );
  };

  // на случай если тренировка изменилась
  // $rootScope.$on('storage_set', function(event, keys, data) {
  //   if (keys[0] != 'train') return;
  //   angular.forEach($scope.trains, function(train, ind) {
  //     if (train.id == keys[1]) {
  //       $scope.trains[ind] = data;
  //     }
  //   });
  //   $scope.saveCache();
  //   $scope.resetBadges();
  // });

  // отметка о приеме лекарства
  $scope.doseMark = function (dose) {
    dose.loading = true;
    $http.get(api('v2/dose/mark/?id='+dose.id))
      .success(function(data) {
        if (data.status == 'success') {
          dose.is_marked = data.is_marked;
          dose.marked_at = data.marked_at;
          $scope.saveCache();
          $scope.resetBadges();
        } else if (data.status == 'error') {
          if (data.type == 'auth_error') {
            $rootScope.logout();
          }
          $rootScope.showError(data.message);
        }
      })
      .error(function () {
        $rootScope.showError('Ошибка сети');
      })
      .finally(function () {
        dose.loading = false;
      });
  };

  $scope.toDay();
});
