angular.module('userSelectProvider', [])
.provider('userSelect', function () {
  this.$get = ['$rootScope', '$ionicModal', 'ionicDatePicker', '$ionicLoading', '$http',
    function ($rootScope, $ionicModal, ionicDatePicker, $ionicLoading, $http) {

    var $scope = $rootScope.$new();
    $scope.data = {};
    $scope.data.query = '';

    $ionicModal.fromTemplateUrl('templates/user/select.html', {
      scope: $scope,
      animation: 'slide-in-down'
    }).then(function (modal) {
      $scope.modal = modal;
    });

    $scope.show = function () {
      $scope.search();
      $scope.modal.show();
    };
    $scope.hide = function () {
      $scope.modal.hide();
    };
    $scope.select = function (user) {
      $scope.callback(user);
      $scope.hide();
    };

    $scope.search = function () {
      $scope.data.search_loading = true;
      $http.get(api('v2/user/search/?q='+$scope.data.query))
        .success(function (data) {
          $scope.user_list = data.list;
        })
        .error(function () {
          $rootScope.showError('Ошибка сети при поиске пользователя.');
        })
        .finally(function () {
          $scope.data.search_loading = false;
        });
    };

    var provider = {};

    provider.init = function (title, callback) {
      $scope.title = title;
      $scope.callback = callback;
      return $scope;
    };

    return provider;

  }];
});
